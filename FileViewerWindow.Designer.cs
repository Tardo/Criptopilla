﻿namespace Criptopilla
{
    partial class FileViewerWindow
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileViewerWindow));
            this.VerBytes = new System.Windows.Forms.DataGridView();
            this.panelDeVer = new System.Windows.Forms.Panel();
            this.OpcionVerHex = new System.Windows.Forms.RadioButton();
            this.OpcionVerDEC = new System.Windows.Forms.RadioButton();
            this.OpcionVerASCII = new System.Windows.Forms.RadioButton();
            this.VerASCII = new System.Windows.Forms.TextBox();
            this.informador = new System.Windows.Forms.StatusStrip();
            this.etiquetaInformativa = new System.Windows.Forms.ToolStripStatusLabel();
            this.VerBytesDec = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.VerBytes)).BeginInit();
            this.panelDeVer.SuspendLayout();
            this.informador.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VerBytesDec)).BeginInit();
            this.SuspendLayout();
            // 
            // VerBytes
            // 
            this.VerBytes.AllowUserToAddRows = false;
            this.VerBytes.AllowUserToDeleteRows = false;
            this.VerBytes.AllowUserToResizeColumns = false;
            this.VerBytes.AllowUserToResizeRows = false;
            this.VerBytes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.VerBytes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.VerBytes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VerBytes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.VerBytes.Location = new System.Drawing.Point(1, 27);
            this.VerBytes.Name = "VerBytes";
            this.VerBytes.ReadOnly = true;
            this.VerBytes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.VerBytes.RowHeadersVisible = false;
            this.VerBytes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.VerBytes.Size = new System.Drawing.Size(512, 227);
            this.VerBytes.TabIndex = 0;
            // 
            // panelDeVer
            // 
            this.panelDeVer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelDeVer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDeVer.Controls.Add(this.OpcionVerHex);
            this.panelDeVer.Controls.Add(this.OpcionVerDEC);
            this.panelDeVer.Controls.Add(this.OpcionVerASCII);
            this.panelDeVer.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDeVer.Location = new System.Drawing.Point(0, 0);
            this.panelDeVer.Name = "panelDeVer";
            this.panelDeVer.Size = new System.Drawing.Size(525, 27);
            this.panelDeVer.TabIndex = 1;
            // 
            // OpcionVerHex
            // 
            this.OpcionVerHex.AutoSize = true;
            this.OpcionVerHex.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpcionVerHex.Location = new System.Drawing.Point(186, 3);
            this.OpcionVerHex.Name = "OpcionVerHex";
            this.OpcionVerHex.Size = new System.Drawing.Size(102, 18);
            this.OpcionVerHex.TabIndex = 2;
            this.OpcionVerHex.Text = "HEXADECIMAL";
            this.OpcionVerHex.UseVisualStyleBackColor = true;
            this.OpcionVerHex.Click += new System.EventHandler(this.OnClickRadioButtonOption);
            // 
            // OpcionVerDEC
            // 
            this.OpcionVerDEC.AutoSize = true;
            this.OpcionVerDEC.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpcionVerDEC.Location = new System.Drawing.Point(448, 3);
            this.OpcionVerDEC.Name = "OpcionVerDEC";
            this.OpcionVerDEC.Size = new System.Drawing.Size(74, 18);
            this.OpcionVerDEC.TabIndex = 2;
            this.OpcionVerDEC.Text = "DECIMAL";
            this.OpcionVerDEC.UseVisualStyleBackColor = true;
            this.OpcionVerDEC.Click += new System.EventHandler(this.OnClickRadioButtonOption);
            // 
            // OpcionVerASCII
            // 
            this.OpcionVerASCII.AutoSize = true;
            this.OpcionVerASCII.Checked = true;
            this.OpcionVerASCII.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpcionVerASCII.Location = new System.Drawing.Point(11, 3);
            this.OpcionVerASCII.Name = "OpcionVerASCII";
            this.OpcionVerASCII.Size = new System.Drawing.Size(60, 18);
            this.OpcionVerASCII.TabIndex = 2;
            this.OpcionVerASCII.TabStop = true;
            this.OpcionVerASCII.Text = "ASCII";
            this.OpcionVerASCII.UseVisualStyleBackColor = true;
            this.OpcionVerASCII.Click += new System.EventHandler(this.OnClickRadioButtonOption);
            // 
            // VerASCII
            // 
            this.VerASCII.BackColor = System.Drawing.SystemColors.Window;
            this.VerASCII.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VerASCII.Location = new System.Drawing.Point(0, 27);
            this.VerASCII.Multiline = true;
            this.VerASCII.Name = "VerASCII";
            this.VerASCII.ReadOnly = true;
            this.VerASCII.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.VerASCII.Size = new System.Drawing.Size(331, 208);
            this.VerASCII.TabIndex = 2;
            this.VerASCII.WordWrap = false;
            // 
            // informador
            // 
            this.informador.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.etiquetaInformativa});
            this.informador.Location = new System.Drawing.Point(0, 269);
            this.informador.Name = "informador";
            this.informador.Size = new System.Drawing.Size(525, 22);
            this.informador.TabIndex = 3;
            this.informador.Text = "statusStrip1";
            // 
            // etiquetaInformativa
            // 
            this.etiquetaInformativa.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaInformativa.Name = "etiquetaInformativa";
            this.etiquetaInformativa.Size = new System.Drawing.Size(0, 17);
            // 
            // VerBytesDec
            // 
            this.VerBytesDec.AllowUserToAddRows = false;
            this.VerBytesDec.AllowUserToDeleteRows = false;
            this.VerBytesDec.AllowUserToResizeColumns = false;
            this.VerBytesDec.AllowUserToResizeRows = false;
            this.VerBytesDec.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.VerBytesDec.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.VerBytesDec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VerBytesDec.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.VerBytesDec.Location = new System.Drawing.Point(1, 27);
            this.VerBytesDec.Name = "VerBytesDec";
            this.VerBytesDec.ReadOnly = true;
            this.VerBytesDec.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.VerBytesDec.RowHeadersVisible = false;
            this.VerBytesDec.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.VerBytesDec.Size = new System.Drawing.Size(512, 227);
            this.VerBytesDec.TabIndex = 0;
            // 
            // Ver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 291);
            this.Controls.Add(this.informador);
            this.Controls.Add(this.VerASCII);
            this.Controls.Add(this.panelDeVer);
            this.Controls.Add(this.VerBytesDec);
            this.Controls.Add(this.VerBytes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Ver";
            this.Text = "Ver";
            this.SizeChanged += new System.EventHandler(this.OnSizeChangedFileViewerWindow);
            ((System.ComponentModel.ISupportInitialize)(this.VerBytes)).EndInit();
            this.panelDeVer.ResumeLayout(false);
            this.panelDeVer.PerformLayout();
            this.informador.ResumeLayout(false);
            this.informador.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VerBytesDec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView VerBytes;
        private System.Windows.Forms.Panel panelDeVer;
        private System.Windows.Forms.RadioButton OpcionVerHex;
        private System.Windows.Forms.RadioButton OpcionVerDEC;
        private System.Windows.Forms.RadioButton OpcionVerASCII;
        private System.Windows.Forms.TextBox VerASCII;
        private System.Windows.Forms.StatusStrip informador;
        private System.Windows.Forms.ToolStripStatusLabel etiquetaInformativa;
        private System.Windows.Forms.DataGridView VerBytesDec;
    }
}