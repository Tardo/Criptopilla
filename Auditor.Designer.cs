﻿namespace Criptopilla
{
    sealed partial class Auditor
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            consola = new System.Windows.Forms.TextBox();
            SuspendLayout();
            // 
            // consola
            // 
            consola.BackColor = System.Drawing.SystemColors.WindowFrame;
            consola.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            consola.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            consola.ForeColor = System.Drawing.SystemColors.ScrollBar;
            consola.Location = new System.Drawing.Point(12, 12);
            consola.Multiline = true;
            consola.Name = "consola";
            consola.ReadOnly = true;
            consola.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            consola.Size = new System.Drawing.Size(268, 249);
            consola.TabIndex = 0;
            // 
            // Auditor
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(292, 273);
            ControlBox = false;
            Controls.Add(consola);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Name = "Auditor";
            ShowIcon = false;
            ShowInTaskbar = false;
            Text = "Auditor";
            Resize += new System.EventHandler(Redimensiona);
            ResumeLayout(false);
            PerformLayout();

        }

        #endregion

        private static System.Windows.Forms.TextBox consola;
    }
}