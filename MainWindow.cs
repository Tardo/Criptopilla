﻿/*
    Criptopilla 2014 - http://www.lospillaos.es
 
    En este archivo han contribuido:
      - WhiteSkull
      - unsigned char*
*/
using System;
using System.IO;
using System.Text;
using System.Drawing;
using CriptopillaExt;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Management;
using System.Diagnostics;
using System.Collections;
using System.Threading;

namespace Criptopilla
{
    enum ORDER { ASC=0, DESC=1 };

    public partial class MainWindow : Form
    {
        //private Auditor auditor = new Auditor();
        private Bitmap          m_ImageColumnOrder;
        public delegate void    delegateAuditor(string texto);
        private delegate void   delegateUpdater(bool showStatus);
        private Thread          m_ThreadFolderChanges;
        private int             m_ColumnIndexSelected = -1;
        private string          m_CurrentPath;
        private string          m_BackFolder;
        private string          m_OSName = "";

        public MainWindow()
        {
            InitializeComponent();
        }




        ////////////////////////
        // LISTVIEW METHODS
        ////////////////////////

        /// <summary>
        /// Obtiene un elemento de la lista por su nombre
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        private ListViewItem GetListItemByName(string itemName)
        {
            foreach (ListViewItem item in FileFolderBrowser.Items)
            {
                if (item.Text.CompareTo(itemName) == 0)
                    return item;
            }

            return null;
        }

        /// <summary>
        /// Comprueba cambios en el directorio actual
        /// </summary>
        /// <param name="showStatus">Si es 'true' reflejará el estado de la busqueda en la progressbar</param>
        private void FillFileFolderBrowser(bool showStatus) // FIXME: Si se cambian los nombres
        {
            bool needRefresh = false;

            if (showStatus)
            {
                ProgressBarFileFolderBrowserStatus.Value = 0;
                ProgressBarFileFolderBrowserStatus.Maximum = Directory.GetFiles(m_CurrentPath).Length + Directory.GetDirectories(m_CurrentPath).Length;
            }

            // Borrar los registros que ya no existen
            foreach (ListViewItem item in FileFolderBrowser.Items)
            {
                String fullPath = m_CurrentPath + "\\" + item.Text;

                if (!Directory.Exists(fullPath) && !File.Exists(fullPath))
                {
                    //if (!File.Exists(item.Text))
                    //    iconos.Images.RemoveAt(item.ImageIndex);
                    FileFolderBrowser.Items.RemoveAt(FileFolderBrowser.Items.IndexOf(item));

                    needRefresh = true;
                }
            }

            // Añadir nuevas carpetas
            foreach (string folderPath in Directory.GetDirectories(m_CurrentPath))
            {
                String folderName = folderPath.Substring(folderPath.LastIndexOf("\\") + 1);
                ListViewItem item = GetListItemByName(folderName);

                if (item == null)
                {
                    Auditor.printMessage("Añadido el directorio '" + folderPath + "'...");
                        
                    FileFolderBrowser.Items.Add(folderName, folderName, 0).UseItemStyleForSubItems = false;
                    FileFolderBrowser.Items[FileFolderBrowser.Items.Count - 1].ImageIndex = 0;
                    FileFolderBrowser.Items[FileFolderBrowser.Items.Count - 1].SubItems.AddRange(new string[] {
                        "",
                        "Carpeta de archivos",
                        CriptoSystem.GetTimeString(new FileInfo(folderPath).LastWriteTime)
                    });

                    needRefresh = true;
                }

                if (showStatus)
                    ProgressBarFileFolderBrowserStatus.Value++;
            }

            // Añadir nuevos archivos
            foreach (string filePath in Directory.GetFiles(m_CurrentPath))
            {
                String fileName = filePath.Substring(filePath.LastIndexOf("\\") + 1);

                ListViewItem item = GetListItemByName(fileName);
                if (item == null)
                {
                    Auditor.printMessage("Añadido el archivo '" + filePath + "'...");

                    Icon exeIcon = System.Drawing.Icon.ExtractAssociatedIcon(filePath);

                    // Añadir Icono Grande
                    LargeIconsList.Images.Add(exeIcon);

                    // Crear Icono de 16x16
                    Bitmap bm = new Bitmap(exeIcon.ToBitmap());
                    Bitmap thumb = new Bitmap(16, 16);
                    Graphics g = Graphics.FromImage(thumb);
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(bm, new Rectangle(0, 0, 16, 16), new Rectangle(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel);
                    g.Dispose();
                    bm.Dispose();
                    SmallIconsList.Images.Add(thumb);

                    FileFolderBrowser.Items.Add(fileName, fileName, 0).UseItemStyleForSubItems = false;
                    FileFolderBrowser.Items[FileFolderBrowser.Items.Count - 1].ImageIndex = SmallIconsList.Images.Count - 1;
                    FileFolderBrowser.Items[FileFolderBrowser.Items.Count - 1].SubItems.AddRange(new string[] {
                        new FileInfo(filePath).Length.ToString("N0"),
                        "Fichero " + CriptoSystem.GetFileTypeByExtension(new FileInfo(filePath).Extension),
                        CriptoSystem.GetTimeString(new FileInfo(filePath).LastWriteTime)
                    });

                    needRefresh = true;
                }

                if (showStatus)
                    ProgressBarFileFolderBrowserStatus.Value++;
            }

            if (needRefresh)
            {
                OnSelectedItemFileFolderBrowser(this, null);
                FileFolderBrowser.SmallImageList = SmallIconsList;
                FileFolderBrowser.LargeImageList = LargeIconsList;
                FileFolderBrowser.Refresh();

                Auditor.printMessage("Modificaciones en el directorio " + m_CurrentPath);
                Auditor.printMessage("T. elementos= " + (Directory.GetFiles(m_CurrentPath).Length + Directory.GetDirectories(m_CurrentPath).Length) + " T. elementos (explorador)="+FileFolderBrowser.Items.Count);
            }
        }

        /// <summary>
        /// Limpia la ListView 'FileFolderBrowser'
        /// </summary>
        private void ClearFileFolderBrowser()
        {
            FileFolderBrowser.Items.Clear();

            foreach (Bitmap smallIcon in SmallIconsList.Images)
                smallIcon.Dispose();

            foreach (Bitmap largeIcon in LargeIconsList.Images)
                largeIcon.Dispose();

            SmallIconsList.Images.Clear();
            LargeIconsList.Images.Clear();
            SmallIconsList.Images.Add(Criptopilla.Properties.Resources.carpeta);
            LargeIconsList.Images.Add(Criptopilla.Properties.Resources.carpeta);
        }




        ////////////////////////
        // MAIN METHODS
        ////////////////////////

        /// <summary>
        /// Actualiza el navegador de archivos
        /// </summary>
        private void UpdateFileFolderBrowser()
        {
            ordenarPorNombre.CheckState = CheckState.Unchecked;
            ordenarPorModificado.CheckState = CheckState.Unchecked;
            ordenarPorTamaño.CheckState = CheckState.Unchecked;
            ordenarPorTipo.CheckState = CheckState.Unchecked;
            m_ColumnIndexSelected = -1;
            marcaDelMetodoDeOrdenacion.Visible = false;

            this.Refresh();

            if (ComboBoxPathSelector.Text.CompareTo("..") == 0) // en caso de usar solamente los dos puntos para descender
            {
                OnClickButtonBackFolder(this, null);
                return;
            }
            else if (ComboBoxPathSelector.Text.IndexOf("\\") <= 0 && ComboBoxPathSelector.Text.IndexOf(":") <= 0)
            { // en caso de que solamente escribamos el nombre de un subdirectorio presente
                foreach (string folderPath in Directory.GetDirectories(m_CurrentPath))
                {
                    if (folderPath.Substring(folderPath.LastIndexOf("\\") + 1).ToLower() == ComboBoxPathSelector.Text.ToLower())
                    {
                        ComboBoxPathSelector.Text = folderPath;
                        UpdateFileFolderBrowser();
                        return;
                    }
                }

                if (Directory.Exists(m_CurrentPath.Substring(0, 2) + ComboBoxPathSelector.Text))
                {
                    //mostradorDeRutas.AutoCompleteCustomSource= ;
                    ComboBoxPathSelector.Text = m_CurrentPath.Substring(0, 2) + ComboBoxPathSelector.Text;
                    UpdateFileFolderBrowser();
                }
                else
                    // No se encontro ningun directorio con ese nombre
                    MessageBox.Show("No es posible acceder a '" + ComboBoxPathSelector.Text + "'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            } // acceder a una unidad directamente
            else if (ComboBoxPathSelector.Text.Length < 3) ComboBoxPathSelector.Text += "\\";
            else if (!Directory.Exists(ComboBoxPathSelector.Text)) // comprobar si existe
            {
                MessageBox.Show("No es posible acceder a '" + ComboBoxPathSelector.Text + "'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Desde aqui todo bien... la ruta es válida
            m_BackFolder = m_CurrentPath;
            Environment.CurrentDirectory = m_CurrentPath = ComboBoxPathSelector.Text;
            this.Text = "Criptopilla v"+CriptoSystem.VERSION+" -- " + m_CurrentPath;

            // recupera directorios
            ClearFileFolderBrowser();
            FillFileFolderBrowser(true);

            StatusBarFileFolderBrowser.Text = "" + FileFolderBrowser.Items.Count + " elementos ";
        }



        ////////////////////////
        // EVENT METHODS
        ////////////////////////

        private void OnLoadMainWindow(object sender, EventArgs e)
        {
            Auditor.GetInstance.Show();
            Auditor.printMessage("*Cargando Ventana Principal*");

            // Averiguamos el SO donde se ejecuta nuestra aplicacion
            try
            {
                foreach (ManagementBaseObject x in new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem").Get())
                    m_OSName = x.GetPropertyValue("Caption").ToString();
            }
            catch (ManagementException me)
            {
                CriptoSystem.ShowMessage(this, "Fue imposible detectar el sistema operativo. El programa se cerrará.\n" + me.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            
            // directorio actual
            m_CurrentPath = ComboBoxPathSelector.Text = m_BackFolder = Directory.GetCurrentDirectory();

            UpdateFileFolderBrowser();

            // Comprobamos periodicamente el directorio actual
            if (m_ThreadFolderChanges != null && m_ThreadFolderChanges.IsAlive) 
                m_ThreadFolderChanges.Abort();
            else
            {
                // Hilo que comprueba cada segundo si hay nuevos elementos o que ya no 
                // estan...
                m_ThreadFolderChanges = 
                new Thread(new ThreadStart(delegate(){
                    while (true)
                    {
                        if (FileFolderBrowser != null && m_CurrentPath.Length > 0)
                            this.Invoke(new delegateUpdater(FillFileFolderBrowser), false);

                        Thread.Sleep(1000); // Cada segundo va comprobarlo
                    }
                }));

                m_ThreadFolderChanges.Start();
            } 
            
            // Actualiza los controles de la ventana
            OnSizeChangeMainWindow(this, null);

            // Introducimos las unidades al elemento cambiarUnidad del menuContextual
            CorrigeUnidades(cambiarDeUnidad, null);
            Auditor.printMessage("*fin Carga Ventana Principal*");
        }

        private void OnSizeChangeMainWindow(object sender, EventArgs e)
        {
            Auditor.printMessage("*Dimension Modificada*");
            
            Auditor.GetInstance.Location = new Point(this.Location.X, this.Height +this.Location.Y);
            Auditor.GetInstance.Width = this.Width;
            
            StatusBarFileFolderBrowser.Width = this.Width -220;
            ComboBoxPathSelector.Width = this.Width -165;         
            FileFolderBrowser.Width = this.Width -103;
            FileFolderBrowser.Height = this.Height - 85;

            if (m_ColumnIndexSelected != -1)
                OnColumnSizeChangeFileFolderBrowser(FileFolderBrowser, new ColumnWidthChangedEventArgs(m_ColumnIndexSelected));

            Auditor.printMessage("*fin Dimension Modificada*");
        }

        private void OnCloseMainWindow(object sender, FormClosingEventArgs e)
        {
            //Thread.CurrentThread.Abort();

            m_ThreadFolderChanges.Abort();
        }

        private void OnClickButtonBackFolder(object sender, EventArgs e)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(m_CurrentPath);
            if (dirInfo.Parent != null)
            {
                ComboBoxPathSelector.Text = dirInfo.Parent.FullName;
                UpdateFileFolderBrowser();
            }
        }

        private void OnClickButtonNextFolder(object sender, EventArgs e)
        {
            ComboBoxPathSelector.Text = m_BackFolder;
            UpdateFileFolderBrowser();
        }

        private void OnInputTextPath(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) UpdateFileFolderBrowser();
        }

        private void OnShowDropboxPath(object sender, EventArgs e)
        {
            ComboBoxPathSelector.Items.Clear();
            foreach (string unidad in Environment.GetLogicalDrives())
                ComboBoxPathSelector.Items.Add(unidad);
        }

        private void OnClickComboBoxPath(object sender, EventArgs e)
        {
            Auditor.printMessage("*Unidad_Click*");

            ComboBoxPathSelector.Text = ((ToolStripMenuItem)sender).Text;

            Auditor.printMessage("   " + ComboBoxPathSelector.Text);
            UpdateFileFolderBrowser();
                
            Auditor.printMessage("*fin Unidad_Click*");
        }

        private void OnSelectDropboxPath(object sender, EventArgs e)
        {
            UpdateFileFolderBrowser();
        }

        private void OnDClickFileFolderBrowser(object sender, EventArgs e)
        {
            String selectedFilename = FileFolderBrowser.SelectedItems[0].Text;

            if (File.Exists(selectedFilename))
                System.Diagnostics.Process.Start(selectedFilename);
            else
            {
                ComboBoxPathSelector.Text = selectedFilename;
                UpdateFileFolderBrowser();
            }
        }

        // OrdenaColumna, es un evento que recoje cuando se hace click en la cabecera
        // de la columna.
        private void OrdenaColumna(object sender, ColumnClickEventArgs e)
        {
            Auditor.printMessage("*OrdenaColumna*");
            Auditor.printMessage("   Columna=" + e.Column.ToString());
            // Se que existen muchos métodos http://support.microsoft.com/kb/319401, 
            // pero esta forma es la única que no da ningún problema... 
            // Crear una estructura con 4 columnas igual que el listview
            // aplicando la formula http://www.csharp-examples.net/sort-array/
            ExplorerItem[] elementos = new ExplorerItem[FileFolderBrowser.Items.Count];

            // Volcamos la informacion del ListView a una estrcutura contenedora
            for (int i = 0; i < FileFolderBrowser.Items.Count; i++)
            {
                elementos[i] = new ExplorerItem();
                elementos[i].Selected = FileFolderBrowser.Items[i].Selected;
                elementos[i].IImagen = FileFolderBrowser.Items[i].ImageIndex;
                elementos[i].Nombre = FileFolderBrowser.Items[i].SubItems[0].Text;
                if (FileFolderBrowser.Items[i].SubItems[1].Text != "")
                    elementos[i].Tamaño = Convert.ToInt64(FileFolderBrowser.Items[i].SubItems[1].Text.Replace(".", ""));
                else elementos[i].Tamaño = 0;
                elementos[i].Tipo = FileFolderBrowser.Items[i].SubItems[2].Text;
                // Fuera de rango [Posible ERROR]
                if (FileFolderBrowser.Items[i].SubItems.Count>3)
                    elementos[i].Modificado = Convert.ToDateTime(FileFolderBrowser.Items[i].SubItems[3].Text);
            }

            // Ordenamos por criterio
            switch (e.Column)
            {
                case (1): // Tamaño
                    if (Convert.ToInt32(FileFolderBrowser.Columns[e.Column].Tag) == (int)ORDER.ASC)
                        Array.Sort(elementos, delegate(ExplorerItem elemento1, ExplorerItem elemento2)
                        { return elemento1.Tamaño.CompareTo(elemento2.Tamaño); });
                    else
                        Array.Sort(elementos, delegate(ExplorerItem elemento1, ExplorerItem elemento2)
                        { return elemento2.Tamaño.CompareTo(elemento1.Tamaño); });
                    break;
                case (2): // Tipo
                    if (Convert.ToInt32(FileFolderBrowser.Columns[e.Column].Tag) == (int)ORDER.ASC)
                        Array.Sort(elementos, delegate(ExplorerItem elemento1, ExplorerItem elemento2)
                        { return elemento1.Tipo.CompareTo(elemento2.Tipo); });
                    else
                        Array.Sort(elementos, delegate(ExplorerItem elemento1, ExplorerItem elemento2)
                        { return elemento2.Tipo.CompareTo(elemento1.Tipo); });
                    break;
                case (3): // Modificado
                    if (Convert.ToInt32(FileFolderBrowser.Columns[e.Column].Tag) == (int)ORDER.ASC)
                        Array.Sort(elementos, delegate(ExplorerItem elemento1, ExplorerItem elemento2)
                        { return elemento1.Modificado.CompareTo(elemento2.Modificado); });
                    else
                        Array.Sort(elementos, delegate(ExplorerItem elemento1, ExplorerItem elemento2)
                        { return elemento2.Modificado.CompareTo(elemento1.Modificado); });
                    break;
                default: // Por Defecto Ordena por el nombre
                    if (Convert.ToInt32(FileFolderBrowser.Columns[e.Column].Tag) == (int)ORDER.ASC)
                        Array.Sort(elementos, delegate(ExplorerItem elemento1, ExplorerItem elemento2)
                        { return elemento1.Nombre.CompareTo(elemento2.Nombre); });
                    else
                        Array.Sort(elementos, delegate(ExplorerItem elemento1, ExplorerItem elemento2)
                        { return elemento1.Nombre.CompareTo(elemento1.Nombre); });
                    break;
            }

            // Devolvemos la informacion ordenada al ListView
            for (int i = 0; i < FileFolderBrowser.Items.Count; i++)
            {
                
                FileFolderBrowser.Items[i].Selected = elementos[i].Selected;
                FileFolderBrowser.Items[i].ImageIndex = elementos[i].IImagen;
                FileFolderBrowser.Items[i].SubItems[0].Text = elementos[i].Nombre;
                if (File.Exists(elementos[i].Nombre))//(elementos[i].Tipo != "Carpeta de archivos")
                    FileFolderBrowser.Items[i].SubItems[1].Text = elementos[i].Tamaño.ToString("N0");
                else FileFolderBrowser.Items[i].SubItems[1].Text = "";
                FileFolderBrowser.Items[i].SubItems[2].Text = elementos[i].Tipo;
                // ERROR
                if (FileFolderBrowser.Items[i].SubItems.Count>3) FileFolderBrowser.Items[i].SubItems[3].Text = elementos[i].Modificado.ToString("dd/MM/yyyy hh:mm");

                for (int c = 0; c < FileFolderBrowser.Columns.Count; c++)
                    if (c != e.Column)
                    {
                        if (FileFolderBrowser.Items[i].SubItems.Count > 3) FileFolderBrowser.Items[i].SubItems[c].BackColor = Color.White;
                    }// salta Excepcion ordenado por "modificado"
                    else FileFolderBrowser.Items[i].SubItems[e.Column].BackColor = Color.LightCyan;
            }

            // Lo que era AScendente ahora es Descendente y/o viceversa
            if (Convert.ToInt32(FileFolderBrowser.Columns[e.Column].Tag) == (int)ORDER.ASC)
                FileFolderBrowser.Columns[e.Column].Tag = ORDER.DESC;
            else FileFolderBrowser.Columns[e.Column].Tag = ORDER.ASC;

            // buscamos la posicion para la marca
            if (verComoDetalles.CheckState==CheckState.Checked)
            {
                int mueveMarca = FileFolderBrowser.Location.X;
                for (int c = 0; c < e.Column; c++)
                    mueveMarca += FileFolderBrowser.Columns[c].Width;

                // Capturamos en caso de no haber capturado sobre el mismo sitio
                if (m_ColumnIndexSelected != e.Column)
                {
                    ActualizaMarca(e.Column, mueveMarca);
                    marcaDelMetodoDeOrdenacion.Location = new Point(mueveMarca + 1, FileFolderBrowser.Location.Y + 1);
                }

                if (m_ImageColumnOrder != null)
                {
                    marcaDelMetodoDeOrdenacion.Image = new Bitmap(m_ImageColumnOrder);
                    if (Convert.ToInt32(FileFolderBrowser.Columns[e.Column].Tag) != (int)ORDER.ASC)
                        Graphics.FromImage(marcaDelMetodoDeOrdenacion.Image)
                        .DrawImageUnscaled(Criptopilla.Properties.Resources.flechaDeOrdenacionAsc,
                               new Point((marcaDelMetodoDeOrdenacion.Width / 2) - Criptopilla.Properties.Resources.flechaDeOrdenacionAsc.Width / 2, 0));
                    else
                        Graphics.FromImage(marcaDelMetodoDeOrdenacion.Image)
                        .DrawImageUnscaled(Criptopilla.Properties.Resources.flechaDeOrdenacionDes,
                               new Point((marcaDelMetodoDeOrdenacion.Width / 2) - Criptopilla.Properties.Resources.flechaDeOrdenacionDes.Width / 2, 0));

                    marcaDelMetodoDeOrdenacion.Refresh();
                }

            }
            
            m_ColumnIndexSelected = e.Column;

            Auditor.printMessage("*fin OrdenaColumna*");
        }

        private void OnColumnSizeChangeFileFolderBrowser(object sender, ColumnWidthChangedEventArgs e)
        {
            // si hay una proxima version :) , se intentarán corregir los fallos
            // de visualizacion de la marcaDeOrdenacion, no puedo estancarme tanto tiempo con estas chorraditas...
            if (m_ColumnIndexSelected != -1)
            {
                // buscamos la posicion para la marca
                int mueveMarca = FileFolderBrowser.Location.X;
                for (int c = 0; c < m_ColumnIndexSelected; c++)
                    mueveMarca += FileFolderBrowser.Columns[c].Width;
                //if (columnaSeleccionada == e.ColumnIndex)
                    ActualizaMarca(/*e.ColumnIndex*/m_ColumnIndexSelected, mueveMarca);
                marcaDelMetodoDeOrdenacion.Location = new Point(mueveMarca + 1, FileFolderBrowser.Location.Y + 1);
                marcaDelMetodoDeOrdenacion.Image = new Bitmap(m_ImageColumnOrder);

                if (Convert.ToInt32(FileFolderBrowser.Columns[m_ColumnIndexSelected].Tag) != (int)ORDER.ASC)
                    Graphics.FromImage(marcaDelMetodoDeOrdenacion.Image)
                    .DrawImageUnscaled(Criptopilla.Properties.Resources.flechaDeOrdenacionAsc,
                           new Point((marcaDelMetodoDeOrdenacion.Width / 2) - Criptopilla.Properties.Resources.flechaDeOrdenacionAsc.Width / 2, 0));
                else
                    Graphics.FromImage(marcaDelMetodoDeOrdenacion.Image)
                    .DrawImageUnscaled(Criptopilla.Properties.Resources.flechaDeOrdenacionDes,
                           new Point((marcaDelMetodoDeOrdenacion.Width / 2) - Criptopilla.Properties.Resources.flechaDeOrdenacionDes.Width / 2, 0));

                marcaDelMetodoDeOrdenacion.Refresh();
            }
            
        }

        private void ActualizaMarca(int columna,int mueveMarca)
        {
            Auditor.printMessage("*ActualizaMArca*");
            int corrigeX = 0, corrigeY = 0,
                corrigeAncho = 0, corrigeAlto = 0;

            Auditor.printMessage("   Columna=" + columna.ToString());
            
            if(m_OSName.IndexOf("Vista")>=0){    
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Appearance",false);

                if (key.GetValue(@"Current").ToString() != "@themeui.dll,-855" &&
                    key.GetValue(@"Current").ToString() != "@themeui.dll,-854")
                {
                    Auditor.printMessage("Apariencia Aero");
                }
                else 
                {
                    corrigeAlto = 9; corrigeAncho = 2;
                    corrigeY = 5; corrigeX = 4;
                    Auditor.printMessage("Apariencia Estandar");
                }
            }else // XP o inferior
            {
                corrigeAlto = 9; corrigeAncho = 2;
                corrigeY = 5; corrigeX = 4;
                Auditor.printMessage("Windows XP o peor");
            }

            m_ImageColumnOrder = new Bitmap((FileFolderBrowser.Columns[columna].Width + 1)-corrigeAncho, 24-corrigeAlto);
            Graphics graphics = Graphics.FromImage(m_ImageColumnOrder);
            marcaDelMetodoDeOrdenacion.Visible = false;
            this.Refresh();
            Cursor.Hide();
            graphics.CopyFromScreen((mueveMarca + this.Location.X + 9)-corrigeX, (FileFolderBrowser.Location.Y + this.Location.Y + 29)-corrigeY,
            0, 0, m_ImageColumnOrder.Size
            , CopyPixelOperation.MergeCopy);
            Cursor.Show();
            marcaDelMetodoDeOrdenacion.Visible = true;
            marcaDelMetodoDeOrdenacion.Size = m_ImageColumnOrder.Size;
            Auditor.printMessage("*fin Actualiza*");
        }

        // Cuando seleccionamos los elementos del ListView o explorador
        private void OnSelectedItemFileFolderBrowser(object sender, EventArgs e)
        {
            if (FileFolderBrowser.SelectedIndices.Count > 0)
                StatusBarFileFolderBrowser.Text = "" + FileFolderBrowser.SelectedIndices.Count + " elementos seleccionados";
            else StatusBarFileFolderBrowser.Text = "" + FileFolderBrowser.Items.Count + " elementos";
        }

        private void OnClickButtonEncrypt(object sender, EventArgs e)
        {
            if (FileFolderBrowser.SelectedItems.Count > 0)
            {
                ArrayList ficheros = new ArrayList();

                foreach (ListViewItem elemento in FileFolderBrowser.SelectedItems)
                    ficheros.Add(m_CurrentPath + "\\" + elemento.Text);

                //new CriptopillaExt.CriptopillaExt().Encriptar(ref ficheros);
                new EncryptDialog(ref ficheros, m_CurrentPath).ShowDialog();
            }
        }

        // Seleccionamos la opción de desencriptar
        private void OnClickButtonDecrypt(object sender, EventArgs e)
        {
            if (FileFolderBrowser.SelectedItems.Count > 0)
            {
                ArrayList ficheros = new ArrayList();

                foreach (ListViewItem elemento in FileFolderBrowser.SelectedItems)
                    ficheros.Add(m_CurrentPath + "\\" + elemento.Text);

                //new CriptopillaExt.CriptopillaExt().Desencriptar(ref ficheros);
                new EncryptDialog().ChangeToDecrypMode(ref ficheros);
            }
        }

        private void OnClickButtonOptions(object sender, EventArgs e)
        {

        }

        private void CorrigeUnidades(object sender, EventArgs e)
        {
            string[] unidad = Environment.GetLogicalDrives();
            for (int i = 0; i < unidad.Length; i++)
                if (i < cambiarDeUnidad.DropDownItems.Count)
                {
                    ((ToolStripMenuItem)cambiarDeUnidad.DropDownItems[i]).CheckState=CheckState.Checked;

                    if (((ToolStripMenuItem)cambiarDeUnidad.DropDownItems[i]).Text == m_CurrentPath.Substring(0, 3))
                        ((ToolStripMenuItem)cambiarDeUnidad.DropDownItems[i]).Checked = true;
                    else ((ToolStripMenuItem)cambiarDeUnidad.DropDownItems[i]).Checked = false;

                    cambiarDeUnidad.DropDownItems[i].Text= unidad[i];
                }
                else cambiarDeUnidad.DropDownItems.Add(new ToolStripMenuItem(unidad[i],null,new EventHandler(OnClickComboBoxPath)));

            while (cambiarDeUnidad.DropDownItems.Count > unidad.Length)
                cambiarDeUnidad.DropDownItems.RemoveAt(cambiarDeUnidad.DropDownItems.Count - 1);
        }

        private void OnMoveMainWindow(object sender, EventArgs e)
        {
            Auditor.GetInstance.Location = new Point(this.Location.X, this.Height + this.Location.Y);

        }



        ////////////////////////
        // MENU EVENT METHODS
        ////////////////////////

        private void OnClickMenuItemSelectAll(object sender, EventArgs e)
        {
            foreach (ListViewItem elemento in FileFolderBrowser.Items) elemento.Selected = true;
        }

        private void OnClickMenuItemReverseSelection(object sender, EventArgs e)
        {
            foreach (ListViewItem elemento in FileFolderBrowser.Items) elemento.Selected ^= true;
        }

        private void OnClickMenuItemShowDetails(object sender, EventArgs e)
        {
            if (verComoDetalles.CheckState != CheckState.Checked)
            {
                verComoLista.CheckState = CheckState.Unchecked;
                verComoDetalles.CheckState = CheckState.Checked;
                FileFolderBrowser.View = View.Details;
                if (m_ColumnIndexSelected != -1)
                {
                    marcaDelMetodoDeOrdenacion.Visible = true;
                    OnColumnSizeChangeFileFolderBrowser(FileFolderBrowser, new ColumnWidthChangedEventArgs(m_ColumnIndexSelected));
                }
             }
        }

        private void OnClickMenuItemShowSimpleList(object sender, EventArgs e)
        {
            if (verComoLista.CheckState != CheckState.Checked)
            {
                verComoLista.CheckState = CheckState.Checked;
                verComoDetalles.CheckState = CheckState.Unchecked;
                FileFolderBrowser.View = View.List;
                if (marcaDelMetodoDeOrdenacion.Visible) marcaDelMetodoDeOrdenacion.Visible = false;
            }
        }

        private void OnClickMenuItemShortByName(object sender, EventArgs e)
        {
            ordenarPorNombre.CheckState = CheckState.Checked;
            ordenarPorModificado.CheckState = CheckState.Unchecked;
            ordenarPorTamaño.CheckState = CheckState.Unchecked;
            ordenarPorTipo.CheckState = CheckState.Unchecked;
            OrdenaColumna(FileFolderBrowser, new ColumnClickEventArgs(0));
        }

        private void OnClickMenuItemShortBySize(object sender, EventArgs e)
        {
            ordenarPorNombre.CheckState = CheckState.Unchecked;
            ordenarPorModificado.CheckState = CheckState.Unchecked;
            ordenarPorTamaño.CheckState = CheckState.Checked;
            ordenarPorTipo.CheckState = CheckState.Unchecked;
            OrdenaColumna(FileFolderBrowser, new ColumnClickEventArgs(1));
        }

        private void OnClickMenuItemShortByType(object sender, EventArgs e)
        {
            ordenarPorNombre.CheckState = CheckState.Unchecked;
            ordenarPorModificado.CheckState = CheckState.Unchecked;
            ordenarPorTamaño.CheckState = CheckState.Unchecked;
            ordenarPorTipo.CheckState = CheckState.Checked;
            OrdenaColumna(FileFolderBrowser, new ColumnClickEventArgs(2));
        }

        private void OnClickMenuItemShortByDateModified(object sender, EventArgs e)
        {
            ordenarPorNombre.CheckState = CheckState.Unchecked;
            ordenarPorModificado.CheckState = CheckState.Checked;
            ordenarPorTamaño.CheckState = CheckState.Unchecked;
            ordenarPorTipo.CheckState = CheckState.Unchecked;
            OrdenaColumna(FileFolderBrowser, new ColumnClickEventArgs(3));
        }

        private void OnClickMenuItemDeleteFiles(object sender, EventArgs e)
        {
            Auditor.printMessage("*Eliminar*");

            if (FileFolderBrowser.SelectedItems.Count > 0) // Si hay elemento(s) seleccionado(s)
            {
                if (FileFolderBrowser.SelectedItems.Count > 1) // muchos
                {
                    try
                    {
                        if (MessageBox.Show(this, "¿Está seguro que desea mover estos " + FileFolderBrowser.SelectedIndices.Count + " elementos a la Papelera de reciclaje",
                                "Eliminar elementos multiples",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            foreach (ListViewItem elemento in FileFolderBrowser.SelectedItems)
                            {
                                if (Directory.Exists(elemento.Text)) //(elemento.SubItems[2].Text.IndexOf("Carpeta de archivos") >= 0)
                                    Microsoft.VisualBasic.FileIO.FileSystem.DeleteDirectory(
                                                                                        elemento.Text,
                                                                                        Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs,
                                                                                        Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin);
                                else if (File.Exists(elemento.Text)) 
                                    Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile(
                                                                                    elemento.Text,
                                                                                    Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs,
                                                                                    Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin);
                            }
                        }
                        
                    }
                    catch (System.OperationCanceledException OC)
                    {
                        CriptoSystem.ShowMessage(this, "Ocurrio un error inesperado: " + OC.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    UpdateFileFolderBrowser();
                }
                else // Solo hay uno 
                {
                    try
                    {
                        if (Directory.Exists(FileFolderBrowser.Items[FileFolderBrowser.SelectedIndices[0]].Text)) //(explorador.Items[explorador.SelectedIndices[0]].SubItems[2].Text.IndexOf("Carpeta de archivos") >= 0)
                            Microsoft.VisualBasic.FileIO.FileSystem.DeleteDirectory(
                                                                                FileFolderBrowser.Items[FileFolderBrowser.SelectedIndices[0]].Text,
                                                                                Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                                                                                Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin);
                        else if (File.Exists(FileFolderBrowser.Items[FileFolderBrowser.SelectedIndices[0]].Text)) 
                            Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile(
                                                                            FileFolderBrowser.Items[FileFolderBrowser.SelectedIndices[0]].Text,
                                                                            Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                                                                            Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin);
                    }
                    catch(System.OperationCanceledException OC)
                    {
                        CriptoSystem.ShowMessage(this, "Ocurrio un error inesperado: "+OC.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    UpdateFileFolderBrowser();
                }
            }
            else // No hay elementos seleccionados
            { }
            Auditor.printMessage("*fin Eliminar*");
        }

        private void OnClickMenuItemRenameItem(object sender, LabelEditEventArgs e)
        {
            if (e.Label != null)
            {
                if (e.Label.Length > 1)
                    try
                    {
                        if (File.Exists(((ListView)sender).Items[e.Item].Text))//(((ListView)sender).Items[e.Item].SubItems[2].Text!="Carpeta de archivos")
                            File.Move(m_CurrentPath + "\\" + ((ListView)sender).Items[e.Item].Text
                            , m_CurrentPath + "\\" + e.Label);
                        else if(Directory.Exists(((ListView)sender).Items[e.Item].Text)) 
                            Directory.Move(m_CurrentPath + "\\" + ((ListView)sender).Items[e.Item].Text
                            , m_CurrentPath + "\\" + e.Label);
                    }
                    catch (IOException io)
                    {
                        e.CancelEdit = true;
                        Auditor.printMessage("Error " + io.Message);
                    }
                else e.CancelEdit = true;
            }
            else e.CancelEdit = true;
        }

        private void OnClickMenuItemRenameFiles(object sender, EventArgs e)
        {
            Auditor.printMessage("*Renombrar*");

            if (FileFolderBrowser.SelectedItems.Count > 0) // Si hay elemento(s) seleccionado(s)
                FileFolderBrowser.SelectedItems[FileFolderBrowser.SelectedItems.Count-1].BeginEdit();
            Auditor.printMessage("*fin Renombrar*");
        }

        private void OnClickMenuItemCreateFolder(object sender, EventArgs e)
        {
            Auditor.printMessage("*Crear carpeta*");
            string nombrePorDefecto = "Nueva Carpeta", nombreDelDirectorio = nombrePorDefecto;
            int i = 0;
            string [] directorios=Directory.GetDirectories(m_CurrentPath,nombrePorDefecto);
            while (directorios.Length > 0)
            {
                Auditor.printMessage("Bucle");
                nombreDelDirectorio = nombrePorDefecto+ (++i).ToString();
                directorios = Directory.GetDirectories(m_CurrentPath, nombreDelDirectorio);
            }
                
            Directory.CreateDirectory(nombreDelDirectorio);
            FillFileFolderBrowser(false);
            FileFolderBrowser.Items[FileFolderBrowser.Items.Count - 1].EnsureVisible();
            FileFolderBrowser.Items[FileFolderBrowser.Items.Count - 1].BeginEdit();
            Auditor.printMessage("*fin Crear Carpeta*");
        }

        private void OnClickMenuItemCopyFolder(object sender, EventArgs e)
        {
            System.Collections.Specialized.StringCollection elementos =new System.Collections.Specialized.StringCollection();

            if (FileFolderBrowser.SelectedItems.Count > 0)
                foreach (ListViewItem elemento in FileFolderBrowser.SelectedItems)
                    elementos.Add(m_CurrentPath + "\\" + elemento.Text);
        
            Clipboard.SetFileDropList( elementos );
        }

        private void OnClickMenuItemPasteFiles(object sender, EventArgs e)
        {
            foreach (string fileInPath in Clipboard.GetFileDropList())
            {
                string fileOutPath = m_CurrentPath + "\\" + fileInPath.Substring(fileInPath.LastIndexOf("\\") + 1);

                if (!File.Exists(fileOutPath) && !Directory.Exists(fileOutPath))
                {
                    if (File.Exists(fileInPath)) Microsoft.VisualBasic.FileSystem.FileCopy(fileInPath, fileOutPath);
                    else if (Directory.Exists(fileInPath)) CriptoSystem.DirectoryCopy(fileInPath, fileOutPath, true);
                }
                else
                {
                    string newFileName = "Copia de " + fileInPath.Substring(fileInPath.LastIndexOf("\\") + 1);
                    string newFilePath = m_CurrentPath + "\\" + newFileName;
                    while (File.Exists(newFileName) || Directory.Exists(newFileName))
                        newFileName = "Copia de " + newFileName;
                    if (File.Exists(fileInPath)) Microsoft.VisualBasic.FileSystem.FileCopy(fileInPath, newFilePath);
                    else if (Directory.Exists(fileInPath)) CriptoSystem.DirectoryCopy(fileInPath, newFilePath, true);

                }
            }

            // Pegarlos con un copy normal, el tema es saber si son directorios o ficheros
        }

        private void OnClickMenuItemShowFile(object sender, EventArgs e)
        {
            string fileName = FileFolderBrowser.SelectedItems[FileFolderBrowser.SelectedItems.Count - 1].Text;
            if (Directory.Exists(fileName)) // Si es un directorio, nos vamos a su interior,
            {
                ComboBoxPathSelector.Text = fileName;
                UpdateFileFolderBrowser();
            }
            else // de lo contrario, lo vemos con WhiteSkull Visualizador Muahahaha
            {
                string filePath = m_CurrentPath + "\\" + fileName;
                FileViewerWindow visualizador = new FileViewerWindow(filePath);
                visualizador.Show();
            }
        }

        private void OnClickMenuItemEncryptFiles(object sender, EventArgs e)
        {
            OnClickButtonEncrypt(this, null);
        }

        private void OnClickMenuItemDecryptFiles(object sender, EventArgs e)
        {
            OnClickButtonDecrypt(this, null);
        }

        // WTF: Que hace esto? Intentar copiar desde el clipboard?
        private void CompruebaUnasCosillas(object sender, System.ComponentModel.CancelEventArgs e)
        {
            pegarFicheros.Enabled = Clipboard.ContainsFileDropList();

            if (FileFolderBrowser.SelectedItems.Count < 1)
                eliminarFicheros.Enabled =
                copiarFicheros.Enabled =
                renombrarFichero.Enabled =
                encriptarFicheros.Enabled =
                desencriptarFicheros.Enabled =
                verFichero.Enabled = false;
            else
                eliminarFicheros.Enabled =
                copiarFicheros.Enabled =
                renombrarFichero.Enabled =
                encriptarFicheros.Enabled =
                desencriptarFicheros.Enabled =
                verFichero.Enabled = true;
        }
    }
}

