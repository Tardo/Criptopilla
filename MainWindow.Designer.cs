﻿namespace Criptopilla
{
    partial class MainWindow
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuPrincipal = new System.Windows.Forms.ToolStrip();
            this.ButtonEncrypt = new System.Windows.Forms.ToolStripButton();
            this.ButtonDecrypt = new System.Windows.Forms.ToolStripButton();
            this.ButtonOptions = new System.Windows.Forms.ToolStripButton();
            this.FileFolderBrowser = new System.Windows.Forms.ListView();
            this.nombre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tamaño = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tipo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.modificado = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuContextual = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.seleccionarTodo = new System.Windows.Forms.ToolStripMenuItem();
            this.invertirSeleccion = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.encriptarFicheros = new System.Windows.Forms.ToolStripMenuItem();
            this.desencriptarFicheros = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.crearCarpeta = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarFicheros = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarFicheros = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarFicheros = new System.Windows.Forms.ToolStripMenuItem();
            this.renombrarFichero = new System.Windows.Forms.ToolStripMenuItem();
            this.verFichero = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.verComo = new System.Windows.Forms.ToolStripMenuItem();
            this.verComoLista = new System.Windows.Forms.ToolStripMenuItem();
            this.verComoDetalles = new System.Windows.Forms.ToolStripMenuItem();
            this.ordenarPor = new System.Windows.Forms.ToolStripMenuItem();
            this.ordenarPorNombre = new System.Windows.Forms.ToolStripMenuItem();
            this.ordenarPorTamaño = new System.Windows.Forms.ToolStripMenuItem();
            this.ordenarPorTipo = new System.Windows.Forms.ToolStripMenuItem();
            this.ordenarPorModificado = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarDeUnidad = new System.Windows.Forms.ToolStripMenuItem();
            this.barraDelExplorador = new System.Windows.Forms.ToolStrip();
            this.ButtonBackPath = new System.Windows.Forms.ToolStripButton();
            this.ButtonNextPath = new System.Windows.Forms.ToolStripButton();
            this.separador = new System.Windows.Forms.ToolStripSeparator();
            this.ComboBoxPathSelector = new System.Windows.Forms.ToolStripComboBox();
            this.informador = new System.Windows.Forms.StatusStrip();
            this.StatusBarFileFolderBrowser = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProgressBarFileFolderBrowserStatus = new System.Windows.Forms.ToolStripProgressBar();
            this.SmallIconsList = new System.Windows.Forms.ImageList(this.components);
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.marcaDelMetodoDeOrdenacion = new System.Windows.Forms.PictureBox();
            this.LargeIconsList = new System.Windows.Forms.ImageList(this.components);
            this.menuPrincipal.SuspendLayout();
            this.menuContextual.SuspendLayout();
            this.barraDelExplorador.SuspendLayout();
            this.informador.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marcaDelMetodoDeOrdenacion)).BeginInit();
            this.SuspendLayout();
            // 
            // menuPrincipal
            // 
            this.menuPrincipal.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ButtonEncrypt,
            this.ButtonDecrypt,
            this.ButtonOptions});
            this.menuPrincipal.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.menuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuPrincipal.Name = "menuPrincipal";
            this.menuPrincipal.Size = new System.Drawing.Size(84, 284);
            this.menuPrincipal.Stretch = true;
            this.menuPrincipal.TabIndex = 3;
            // 
            // ButtonEncrypt
            // 
            this.ButtonEncrypt.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEncrypt.Image = global::Criptopilla.Properties.Resources.ico_encrypt;
            this.ButtonEncrypt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButtonEncrypt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonEncrypt.Name = "ButtonEncrypt";
            this.ButtonEncrypt.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.ButtonEncrypt.Size = new System.Drawing.Size(81, 81);
            this.ButtonEncrypt.Text = "Encriptar";
            this.ButtonEncrypt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonEncrypt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ButtonEncrypt.ToolTipText = "Encriptar ficheros seleccionados";
            this.ButtonEncrypt.Click += new System.EventHandler(this.OnClickButtonEncrypt);
            // 
            // ButtonDecrypt
            // 
            this.ButtonDecrypt.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonDecrypt.Image = global::Criptopilla.Properties.Resources.ico_decrypt;
            this.ButtonDecrypt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButtonDecrypt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonDecrypt.Name = "ButtonDecrypt";
            this.ButtonDecrypt.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.ButtonDecrypt.Size = new System.Drawing.Size(81, 81);
            this.ButtonDecrypt.Text = "Desencriptar";
            this.ButtonDecrypt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonDecrypt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ButtonDecrypt.ToolTipText = "Desencriptar ficheros seleccionados";
            this.ButtonDecrypt.Click += new System.EventHandler(this.OnClickButtonDecrypt);
            // 
            // ButtonOptions
            // 
            this.ButtonOptions.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonOptions.Image = global::Criptopilla.Properties.Resources.ico_options;
            this.ButtonOptions.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButtonOptions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonOptions.Name = "ButtonOptions";
            this.ButtonOptions.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.ButtonOptions.Size = new System.Drawing.Size(81, 81);
            this.ButtonOptions.Text = "Opciones";
            this.ButtonOptions.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonOptions.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ButtonOptions.ToolTipText = "Opciones de encriptación";
            this.ButtonOptions.Click += new System.EventHandler(this.OnClickButtonOptions);
            // 
            // FileFolderBrowser
            // 
            this.FileFolderBrowser.BackColor = System.Drawing.SystemColors.Window;
            this.FileFolderBrowser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FileFolderBrowser.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nombre,
            this.tamaño,
            this.tipo,
            this.modificado});
            this.FileFolderBrowser.ContextMenuStrip = this.menuContextual;
            this.FileFolderBrowser.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileFolderBrowser.ForeColor = System.Drawing.SystemColors.WindowText;
            this.FileFolderBrowser.FullRowSelect = true;
            this.FileFolderBrowser.LabelEdit = true;
            this.FileFolderBrowser.Location = new System.Drawing.Point(84, 28);
            this.FileFolderBrowser.Name = "FileFolderBrowser";
            this.FileFolderBrowser.Size = new System.Drawing.Size(565, 226);
            this.FileFolderBrowser.TabIndex = 3;
            this.FileFolderBrowser.UseCompatibleStateImageBehavior = false;
            this.FileFolderBrowser.View = System.Windows.Forms.View.Details;
            this.FileFolderBrowser.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.OnClickMenuItemRenameItem);
            this.FileFolderBrowser.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.OrdenaColumna);
            this.FileFolderBrowser.ColumnWidthChanged += new System.Windows.Forms.ColumnWidthChangedEventHandler(this.OnColumnSizeChangeFileFolderBrowser);
            this.FileFolderBrowser.SelectedIndexChanged += new System.EventHandler(this.OnSelectedItemFileFolderBrowser);
            this.FileFolderBrowser.DoubleClick += new System.EventHandler(this.OnDClickFileFolderBrowser);
            // 
            // nombre
            // 
            this.nombre.Tag = "0";
            this.nombre.Text = "Nombre";
            this.nombre.Width = 152;
            // 
            // tamaño
            // 
            this.tamaño.Tag = "0";
            this.tamaño.Text = "Tamaño";
            this.tamaño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tamaño.Width = 113;
            // 
            // tipo
            // 
            this.tipo.Tag = "0";
            this.tipo.Text = "Tipo";
            this.tipo.Width = 156;
            // 
            // modificado
            // 
            this.modificado.Tag = "0";
            this.modificado.Text = "Modificado";
            this.modificado.Width = 125;
            // 
            // menuContextual
            // 
            this.menuContextual.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.seleccionarTodo,
            this.invertirSeleccion,
            this.toolStripSeparator1,
            this.encriptarFicheros,
            this.desencriptarFicheros,
            this.toolStripSeparator2,
            this.crearCarpeta,
            this.copiarFicheros,
            this.pegarFicheros,
            this.eliminarFicheros,
            this.renombrarFichero,
            this.verFichero,
            this.toolStripSeparator3,
            this.verComo,
            this.ordenarPor,
            this.cambiarDeUnidad});
            this.menuContextual.Name = "menuContextual";
            this.menuContextual.Size = new System.Drawing.Size(186, 308);
            this.menuContextual.Opening += new System.ComponentModel.CancelEventHandler(this.CompruebaUnasCosillas);
            // 
            // seleccionarTodo
            // 
            this.seleccionarTodo.Name = "seleccionarTodo";
            this.seleccionarTodo.Size = new System.Drawing.Size(185, 22);
            this.seleccionarTodo.Text = "Seleccionar todo";
            this.seleccionarTodo.Click += new System.EventHandler(this.OnClickMenuItemSelectAll);
            // 
            // invertirSeleccion
            // 
            this.invertirSeleccion.Name = "invertirSeleccion";
            this.invertirSeleccion.Size = new System.Drawing.Size(185, 22);
            this.invertirSeleccion.Text = "Invertir selección";
            this.invertirSeleccion.Click += new System.EventHandler(this.OnClickMenuItemReverseSelection);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(182, 6);
            // 
            // encriptarFicheros
            // 
            this.encriptarFicheros.Name = "encriptarFicheros";
            this.encriptarFicheros.Size = new System.Drawing.Size(185, 22);
            this.encriptarFicheros.Text = "Encriptar ficheros";
            this.encriptarFicheros.Click += new System.EventHandler(this.OnClickMenuItemEncryptFiles);
            // 
            // desencriptarFicheros
            // 
            this.desencriptarFicheros.Name = "desencriptarFicheros";
            this.desencriptarFicheros.Size = new System.Drawing.Size(185, 22);
            this.desencriptarFicheros.Text = "Desencriptar ficheros";
            this.desencriptarFicheros.Click += new System.EventHandler(this.OnClickMenuItemDecryptFiles);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(182, 6);
            // 
            // crearCarpeta
            // 
            this.crearCarpeta.Name = "crearCarpeta";
            this.crearCarpeta.Size = new System.Drawing.Size(185, 22);
            this.crearCarpeta.Text = "Crear Carpeta";
            this.crearCarpeta.Click += new System.EventHandler(this.OnClickMenuItemCreateFolder);
            // 
            // copiarFicheros
            // 
            this.copiarFicheros.Name = "copiarFicheros";
            this.copiarFicheros.Size = new System.Drawing.Size(185, 22);
            this.copiarFicheros.Text = "Copiar Ficheros";
            this.copiarFicheros.Click += new System.EventHandler(this.OnClickMenuItemCopyFolder);
            // 
            // pegarFicheros
            // 
            this.pegarFicheros.Name = "pegarFicheros";
            this.pegarFicheros.Size = new System.Drawing.Size(185, 22);
            this.pegarFicheros.Text = "Pegar Ficheros";
            this.pegarFicheros.Click += new System.EventHandler(this.OnClickMenuItemPasteFiles);
            // 
            // eliminarFicheros
            // 
            this.eliminarFicheros.Name = "eliminarFicheros";
            this.eliminarFicheros.Size = new System.Drawing.Size(185, 22);
            this.eliminarFicheros.Text = "Eliminar Ficheros";
            this.eliminarFicheros.Click += new System.EventHandler(this.OnClickMenuItemDeleteFiles);
            // 
            // renombrarFichero
            // 
            this.renombrarFichero.Name = "renombrarFichero";
            this.renombrarFichero.Size = new System.Drawing.Size(185, 22);
            this.renombrarFichero.Text = "Renombrar Fichero";
            this.renombrarFichero.Click += new System.EventHandler(this.OnClickMenuItemRenameFiles);
            // 
            // verFichero
            // 
            this.verFichero.Name = "verFichero";
            this.verFichero.Size = new System.Drawing.Size(185, 22);
            this.verFichero.Text = "Ver Fichero";
            this.verFichero.Click += new System.EventHandler(this.OnClickMenuItemShowFile);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(182, 6);
            // 
            // verComo
            // 
            this.verComo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verComoLista,
            this.verComoDetalles});
            this.verComo.Name = "verComo";
            this.verComo.Size = new System.Drawing.Size(185, 22);
            this.verComo.Text = "Ver como";
            // 
            // verComoLista
            // 
            this.verComoLista.Name = "verComoLista";
            this.verComoLista.Size = new System.Drawing.Size(115, 22);
            this.verComoLista.Text = "Lista";
            this.verComoLista.Click += new System.EventHandler(this.OnClickMenuItemShowSimpleList);
            // 
            // verComoDetalles
            // 
            this.verComoDetalles.Checked = true;
            this.verComoDetalles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.verComoDetalles.Name = "verComoDetalles";
            this.verComoDetalles.Size = new System.Drawing.Size(115, 22);
            this.verComoDetalles.Text = "Detalles";
            this.verComoDetalles.Click += new System.EventHandler(this.OnClickMenuItemShowDetails);
            // 
            // ordenarPor
            // 
            this.ordenarPor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ordenarPorNombre,
            this.ordenarPorTamaño,
            this.ordenarPorTipo,
            this.ordenarPorModificado});
            this.ordenarPor.Name = "ordenarPor";
            this.ordenarPor.Size = new System.Drawing.Size(185, 22);
            this.ordenarPor.Text = "Ordenar por";
            // 
            // ordenarPorNombre
            // 
            this.ordenarPorNombre.Name = "ordenarPorNombre";
            this.ordenarPorNombre.Size = new System.Drawing.Size(135, 22);
            this.ordenarPorNombre.Text = "Nombre";
            this.ordenarPorNombre.Click += new System.EventHandler(this.OnClickMenuItemShortByName);
            // 
            // ordenarPorTamaño
            // 
            this.ordenarPorTamaño.Name = "ordenarPorTamaño";
            this.ordenarPorTamaño.Size = new System.Drawing.Size(135, 22);
            this.ordenarPorTamaño.Text = "Tamaño";
            this.ordenarPorTamaño.Click += new System.EventHandler(this.OnClickMenuItemShortBySize);
            // 
            // ordenarPorTipo
            // 
            this.ordenarPorTipo.Name = "ordenarPorTipo";
            this.ordenarPorTipo.Size = new System.Drawing.Size(135, 22);
            this.ordenarPorTipo.Text = "Tipo";
            this.ordenarPorTipo.Click += new System.EventHandler(this.OnClickMenuItemShortByType);
            // 
            // ordenarPorModificado
            // 
            this.ordenarPorModificado.Name = "ordenarPorModificado";
            this.ordenarPorModificado.Size = new System.Drawing.Size(135, 22);
            this.ordenarPorModificado.Text = "Modificado";
            this.ordenarPorModificado.Click += new System.EventHandler(this.OnClickMenuItemShortByDateModified);
            // 
            // cambiarDeUnidad
            // 
            this.cambiarDeUnidad.Name = "cambiarDeUnidad";
            this.cambiarDeUnidad.Size = new System.Drawing.Size(185, 22);
            this.cambiarDeUnidad.Text = "Cambiar de unidad";
            this.cambiarDeUnidad.DropDownOpening += new System.EventHandler(this.CorrigeUnidades);
            // 
            // barraDelExplorador
            // 
            this.barraDelExplorador.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ButtonBackPath,
            this.ButtonNextPath,
            this.separador,
            this.ComboBoxPathSelector});
            this.barraDelExplorador.Location = new System.Drawing.Point(84, 0);
            this.barraDelExplorador.Name = "barraDelExplorador";
            this.barraDelExplorador.Size = new System.Drawing.Size(570, 25);
            this.barraDelExplorador.TabIndex = 4;
            // 
            // ButtonBackPath
            // 
            this.ButtonBackPath.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButtonBackPath.Image = global::Criptopilla.Properties.Resources.flechaVolver;
            this.ButtonBackPath.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonBackPath.Name = "ButtonBackPath";
            this.ButtonBackPath.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.ButtonBackPath.Size = new System.Drawing.Size(23, 22);
            this.ButtonBackPath.ToolTipText = "Bajar a la carpeta inferior";
            this.ButtonBackPath.Click += new System.EventHandler(this.OnClickButtonBackFolder);
            // 
            // ButtonNextPath
            // 
            this.ButtonNextPath.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButtonNextPath.Image = global::Criptopilla.Properties.Resources.flchaSiguiente;
            this.ButtonNextPath.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonNextPath.Name = "ButtonNextPath";
            this.ButtonNextPath.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.ButtonNextPath.Size = new System.Drawing.Size(23, 22);
            this.ButtonNextPath.ToolTipText = "Volver a la carpeta anterior";
            this.ButtonNextPath.Click += new System.EventHandler(this.OnClickButtonNextFolder);
            // 
            // separador
            // 
            this.separador.Name = "separador";
            this.separador.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.separador.Size = new System.Drawing.Size(6, 25);
            // 
            // ComboBoxPathSelector
            // 
            this.ComboBoxPathSelector.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ComboBoxPathSelector.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystemDirectories;
            this.ComboBoxPathSelector.AutoSize = false;
            this.ComboBoxPathSelector.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.ComboBoxPathSelector.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBoxPathSelector.Name = "ComboBoxPathSelector";
            this.ComboBoxPathSelector.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.ComboBoxPathSelector.Size = new System.Drawing.Size(200, 21);
            this.ComboBoxPathSelector.DropDown += new System.EventHandler(this.OnShowDropboxPath);
            this.ComboBoxPathSelector.SelectedIndexChanged += new System.EventHandler(this.OnSelectDropboxPath);
            this.ComboBoxPathSelector.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnInputTextPath);
            // 
            // informador
            // 
            this.informador.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.informador.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBarFileFolderBrowser,
            this.ProgressBarFileFolderBrowserStatus});
            this.informador.Location = new System.Drawing.Point(84, 262);
            this.informador.Name = "informador";
            this.informador.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.informador.Size = new System.Drawing.Size(570, 22);
            this.informador.TabIndex = 5;
            // 
            // StatusBarFileFolderBrowser
            // 
            this.StatusBarFileFolderBrowser.AutoSize = false;
            this.StatusBarFileFolderBrowser.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.StatusBarFileFolderBrowser.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.StatusBarFileFolderBrowser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.StatusBarFileFolderBrowser.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBarFileFolderBrowser.Name = "StatusBarFileFolderBrowser";
            this.StatusBarFileFolderBrowser.Size = new System.Drawing.Size(100, 17);
            this.StatusBarFileFolderBrowser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ProgressBarFileFolderBrowserStatus
            // 
            this.ProgressBarFileFolderBrowserStatus.Name = "ProgressBarFileFolderBrowserStatus";
            this.ProgressBarFileFolderBrowserStatus.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.ProgressBarFileFolderBrowserStatus.Size = new System.Drawing.Size(100, 16);
            // 
            // SmallIconsList
            // 
            this.SmallIconsList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.SmallIconsList.ImageSize = new System.Drawing.Size(16, 16);
            this.SmallIconsList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // marcaDelMetodoDeOrdenacion
            // 
            this.marcaDelMetodoDeOrdenacion.BackColor = System.Drawing.Color.Transparent;
            this.marcaDelMetodoDeOrdenacion.Enabled = false;
            this.marcaDelMetodoDeOrdenacion.Location = new System.Drawing.Point(144, 29);
            this.marcaDelMetodoDeOrdenacion.Name = "marcaDelMetodoDeOrdenacion";
            this.marcaDelMetodoDeOrdenacion.Size = new System.Drawing.Size(12, 10);
            this.marcaDelMetodoDeOrdenacion.TabIndex = 6;
            this.marcaDelMetodoDeOrdenacion.TabStop = false;
            this.marcaDelMetodoDeOrdenacion.Visible = false;
            // 
            // LargeIconsList
            // 
            this.LargeIconsList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.LargeIconsList.ImageSize = new System.Drawing.Size(16, 16);
            this.LargeIconsList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 284);
            this.Controls.Add(this.marcaDelMetodoDeOrdenacion);
            this.Controls.Add(this.informador);
            this.Controls.Add(this.barraDelExplorador);
            this.Controls.Add(this.FileFolderBrowser);
            this.Controls.Add(this.menuPrincipal);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.Text = "Criptopilla v.1.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnCloseMainWindow);
            this.Load += new System.EventHandler(this.OnLoadMainWindow);
            this.LocationChanged += new System.EventHandler(this.OnMoveMainWindow);
            this.Resize += new System.EventHandler(this.OnSizeChangeMainWindow);
            this.menuPrincipal.ResumeLayout(false);
            this.menuPrincipal.PerformLayout();
            this.menuContextual.ResumeLayout(false);
            this.barraDelExplorador.ResumeLayout(false);
            this.barraDelExplorador.PerformLayout();
            this.informador.ResumeLayout(false);
            this.informador.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marcaDelMetodoDeOrdenacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip menuPrincipal;
        private System.Windows.Forms.ListView FileFolderBrowser;
        private System.Windows.Forms.ColumnHeader tamaño;
        private System.Windows.Forms.ColumnHeader tipo;
        private System.Windows.Forms.ColumnHeader modificado;
        private System.Windows.Forms.ToolStripButton ButtonEncrypt;
        private System.Windows.Forms.ToolStrip barraDelExplorador;
        private System.Windows.Forms.ToolStripButton ButtonBackPath;
        private System.Windows.Forms.ToolStripButton ButtonNextPath;
        private System.Windows.Forms.ToolStripButton ButtonDecrypt;
        private System.Windows.Forms.ToolStripButton ButtonOptions;
        private System.Windows.Forms.ToolStripSeparator separador;
        private System.Windows.Forms.ToolStripComboBox ComboBoxPathSelector;
        private System.Windows.Forms.StatusStrip informador;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarFileFolderBrowser;
        private System.Windows.Forms.ToolStripProgressBar ProgressBarFileFolderBrowserStatus;
        private System.Windows.Forms.ImageList SmallIconsList;
        private System.Windows.Forms.ColumnHeader nombre;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ContextMenuStrip menuContextual;
        private System.Windows.Forms.ToolStripMenuItem seleccionarTodo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem encriptarFicheros;
        private System.Windows.Forms.ToolStripMenuItem desencriptarFicheros;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem eliminarFicheros;
        private System.Windows.Forms.ToolStripMenuItem renombrarFichero;
        private System.Windows.Forms.ToolStripMenuItem verFichero;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem ordenarPor;
        private System.Windows.Forms.ToolStripMenuItem ordenarPorNombre;
        private System.Windows.Forms.ToolStripMenuItem ordenarPorTamaño;
        private System.Windows.Forms.ToolStripMenuItem cambiarDeUnidad;
        private System.Windows.Forms.ToolStripMenuItem ordenarPorTipo;
        private System.Windows.Forms.ToolStripMenuItem ordenarPorModificado;
        private System.Windows.Forms.ToolStripMenuItem verComo;
        private System.Windows.Forms.ToolStripMenuItem verComoLista;
        private System.Windows.Forms.ToolStripMenuItem verComoDetalles;
        private System.Windows.Forms.ToolStripMenuItem invertirSeleccion;
        private System.Windows.Forms.ToolStripMenuItem crearCarpeta;
        private System.Windows.Forms.ToolStripMenuItem copiarFicheros;
        private System.Windows.Forms.ToolStripMenuItem pegarFicheros;
        private System.Windows.Forms.PictureBox marcaDelMetodoDeOrdenacion;
        private System.Windows.Forms.ImageList LargeIconsList;
    }
}

