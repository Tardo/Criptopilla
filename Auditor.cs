﻿/*
    Criptopilla 2014 - http://www.lospillaos.es
 
    En este archivo han contribuido:
      - WhiteSkull
      - unsigned char*
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Criptopilla
{
    /// <summary>
    /// Esta clase lanza una ventana para imprimir mensajes de depuración
    /// </summary>
    public sealed partial class Auditor : Form
    {
        private static readonly Auditor m_Instance = new Auditor();
 
        private Auditor()
        {
            InitializeComponent();
        }

        public static Auditor GetInstance
        {
            get
            {
                return m_Instance;
            }
        }


// PUBLIC MEMBERS

        /// <summary>
        /// Imprime un mensaje de depuración
        /// </summary>
        /// <param name="text"></param>
        public static void printMessage(string text)
        {
            consola.Text += "[" + DateTime.Now.ToString("hh:mm:ss") + "] " + text + Environment.NewLine;
            consola.SelectionStart = consola.Text.Length - 1;
            consola.ScrollToCaret();   
        }

        public static void DelegadoEnviaMensaje(string cadena)
        {
            printMessage(cadena);
        }


// PRIVATE MEMBERS

        private void Redimensiona(object sender, EventArgs e)
        {
            consola.Width = this.Width - 25;
            consola.Height = this.Height - 25;
        }
    }
}
