﻿/*
    Criptopilla 2014 - http://www.lospillaos.es
 
    En este archivo han contribuido:
      - WhiteSkull
      - unsigned char*
*/
namespace Criptopilla
{    
    public struct ExplorerItem 
    {
        private bool                m_Selected;
        private int                 m_ImageIndex;
        private string              m_Filename;
        private long                m_Size;
        private string              m_Type;
        private System.DateTime     m_DateModified;

        public bool Selected
        {
            get { return m_Selected; }
            set { m_Selected = value; }
        }

        public int IImagen
        {
            get { return m_ImageIndex; }
            set { m_ImageIndex = value; }
        }

        public string Nombre
        {
            get { return m_Filename; }
            set { m_Filename = value; }
        }

        public long Tamaño
        {
            get { return m_Size; }
            set { m_Size = value; }
        }

        public string Tipo
        {
            get { return m_Type; }
            set { m_Type = value; }
        }

        public System.DateTime Modificado
        {
            get { return m_DateModified; }
            set { m_DateModified = value; }
        }
    }
}
