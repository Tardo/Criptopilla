﻿/*
    Criptopilla 2014 - http://www.lospillaos.es
 
    En este archivo han contribuido:
      - WhiteSkull
      - unsigned char*
*/
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Criptopilla
{
    class CriptoSystem
    {
        public static readonly String VERSION = "2.0";

        /// <summary>
        /// Convierte un DateTime a un string formateado (dd/MM/yyyy hh:mm)
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string GetTimeString(DateTime time)
        {
            return time.ToString("dd/MM/yyyy hh:mm");
        }

        //http://bytes.com/topic/c-sharp/answers/519733-detect-file-type
        // Encuentra el tipo de la aplicacion por su extension, buscando en el registro
        /// <summary>
        /// Obtiene el tipo de archivo segun la extensión indicada
        /// </summary>
        /// <param name="ext"></param>
        /// <returns></returns>
        public static string GetFileTypeByExtension(string ext)
        {
            RegistryKey rKey = null;
            RegistryKey sKey = null;
            string FileType = "";

            try
            {
                rKey = Registry.ClassesRoot;
                sKey = rKey.OpenSubKey(ext);
                if (sKey != null && (string)sKey.GetValue("", ext) != ext)
                {
                    sKey = rKey.OpenSubKey((string)sKey.GetValue("", ext));
                    if (sKey != null) FileType = (string)sKey.GetValue("");
                    else FileType = ext.Substring(ext.LastIndexOf('.') + 1).ToUpper();
                }
                else
                    FileType = ext.Substring(ext.LastIndexOf('.') + 1).ToUpper();
                return FileType;
            }
            finally
            {
                if (sKey != null) sKey.Close();
                if (rKey != null) rKey.Close();
            }
        }


        // Porque cojones no está incluido este metodo en la clase Directory ?
        // http://msdn.microsoft.com/es-es/library/bb762914.aspx
        /// <summary>
        /// Copia un directorio
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="destDirName"></param>
        /// <param name="copySubDirs"></param>
        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
                Directory.CreateDirectory(destDirName);


            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, false);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {

                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        public static float GetFreeMemory()
        {
            return GC.GetTotalMemory(false);
        }

        public static void CreateDirectory(string dirTo, string newDirName)
        {
            string tmpDirName = newDirName;

            int i = 1;
            while (Directory.Exists(tmpDirName))
                tmpDirName = newDirName + " (" + (i++) + ")";

            Directory.CreateDirectory(tmpDirName);
        }

        public static void ShowMessage(IWin32Window win, string msg, string title, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            if (win == null)
                MessageBox.Show(msg, "Criptopilla v" + VERSION + " - " + title, buttons, icon);
            else
                MessageBox.Show(win, msg, "Criptopilla v" + VERSION + " - " + title, buttons, icon);
        }

        public static ArrayList GenerateRecursiveFileList(ArrayList rawFileList)
        {
            // La idea es coger una lista de ficheros, incluido directorios
            // y que desglose esos directorios en ficheros
            ArrayList finalFileList = new ArrayList();
            string[] subFiles;

            foreach (string cFile in rawFileList)
            {
                if (File.Exists(cFile))
                    finalFileList.Add(cFile);
                else if (Directory.Exists(cFile))
                {
                    subFiles = Directory.GetFiles(cFile, "*", SearchOption.AllDirectories);
                    foreach (string subfichero in subFiles)
                        finalFileList.Add(subfichero);
                }
            }

            return (ArrayList)finalFileList.Clone();
        }
    }
}
