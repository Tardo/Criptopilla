﻿/*
    Criptopilla 2014 - http://www.lospillaos.es
 
    En este archivo han contribuido:
      - WhiteSkull
      - unsigned char*
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Criptopilla
{
    enum CriptoFileMethod {
        Aes = 0,
        DES = 1,
        RC2 = 2,
        Rijndael = 3,
        TripleDES = 4
    };

    public struct CriptoFileHeader
    {
        public byte m_Version;          // Version del metodo de encapsulado
        public byte m_EncryptMethod;    // Metodo de Encriptación
        public byte m_CipherMode;       // Modo de Cifrado
        public UInt32 m_NumFiles;       // Indica el numero de ficheros
    };

    class CriptoFileServiceProvider
    {
        public static readonly byte[] FILE_SIGN = new byte[] { 67, 82, 80 };    // Firma Archivo Criptopilla
        private static readonly byte[] PASSWORD_SALT = new byte[] { 0x4C, 0x4F, 0x53, 0x50, 0x49, 0x4C, 0x4C, 0x41, 0x4F, 0x53, 0x2E, 0x45, 0x53 }; // TODO: Este valor es usado en la mezcla de la contraseña aportada por el usuario... otro programa que pueda descifrar no podrá hacerlo sin este 'salt'.
        
        SHA1                m_SHA1Service;       // Para Calcular SHA1Sum
        ICriptoFileService  m_InterfaceService;  // Interface de comunicacion con el Form

        public CriptoFileServiceProvider(ICriptoFileService iService)
        {
            m_SHA1Service = new SHA1CryptoServiceProvider();
            m_InterfaceService = iService;
        }

        public int Decrypt(string filePath, string pathTo, string key)
        {
            // Abrimos el archivo a desencriptar
            BinaryReader fileCriptopilla = new BinaryReader(File.Open(filePath, FileMode.Open));

            // Miramos si es un archivo criptopilla y de serlo cojemos la cabecera
            CriptoFileHeader header = new CriptoFileHeader();
            if (!GetHeader(ref fileCriptopilla, ref header))
                return -1;

            int errors = 0;
            try
            {
                CryptoStream objCryptoStream = null;
                // Declaramos el Algoritmo Simétrico
                SymmetricAlgorithm symAlg = SymmetricAlgorithm.Create(System.Enum.GetName(typeof(CriptoFileMethod), header.m_EncryptMethod));
                if (symAlg == null)
                    return -1;
                // Ponemos el modo de cifrado
                symAlg.Mode = (CipherMode)header.m_CipherMode;

                // Generar Clave y Vector IV
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(key, PASSWORD_SALT);
                byte[] keyGen = pdb.GetBytes(symAlg.KeySize/8);
                byte[] keyIV = pdb.GetBytes(symAlg.BlockSize/8);

                // Emepezamos a desempaquetar y desencriptar los archivos
                for (int iItem = 0; iItem < header.m_NumFiles; iItem++)
                {
                    // Nombre del archivo
                    ArrayList rawFileName = new ArrayList();
                    byte cByte = 0;
                    while ((cByte = (byte)fileCriptopilla.ReadByte()) != 0) rawFileName.Add(cByte);
                    string fileName = System.Text.UTF8Encoding.ASCII.GetString((byte[])rawFileName.ToArray(typeof(byte)));
                    string fileFinalPath = pathTo + fileName;

                    // Tamaño del Archivo Encriptado
                    UInt32 fileSize = fileCriptopilla.ReadUInt32();

                    // Comenzamos escritura del archivo desencriptado
                    BinaryWriter streamFileWrite = new BinaryWriter(File.Open(fileFinalPath, FileMode.Create)); // Archivo de Destino
                    objCryptoStream = new CryptoStream(streamFileWrite.BaseStream, symAlg.CreateDecryptor(keyGen, keyIV), CryptoStreamMode.Write);
                    UInt32 toRead = fileSize;
                    do
                    {
                        // Reservamos memoria segun disponibilidad
                        UInt32 blockMemorySize = 0;
                        UInt32 freeMemory = (UInt32)(CriptoSystem.GetFreeMemory() * 0.90F);
                        if (toRead > freeMemory) blockMemorySize = freeMemory;
                        else blockMemorySize = toRead;

                        // Desencriptamos el archivo
                        byte[] tmpData = new byte[blockMemorySize];
                        int readed = fileCriptopilla.Read(tmpData, 0, tmpData.Length);
                        objCryptoStream.Write(tmpData, 0, readed); // Escribimos en el archivo de destino

                        toRead -= (UInt32)readed;
                        m_InterfaceService.OnCriptoProgress(header.m_EncryptMethod, (UInt32)iItem+1, header.m_NumFiles, fileName, (UInt32)fileSize, fileSize - toRead);

                        tmpData = null;
                    } while (toRead > 0);

                    if (!objCryptoStream.HasFlushedFinalBlock)
                        objCryptoStream.FlushFinalBlock();

                    // SHA1-Sum del Archivo sin encriptar
                    UInt32 sha1SumSize = fileCriptopilla.ReadUInt32();
                    byte[] sha1Sum = fileCriptopilla.ReadBytes((int)sha1SumSize);

                    // Verificar SHA1-Sum
                    streamFileWrite.BaseStream.Position = 0;
                    byte[] fileSHA1Sum = m_SHA1Service.ComputeHash(streamFileWrite.BaseStream);
                    streamFileWrite.Close();

                    // Si el archivo resultante está corrupto se elimina
                    if (!fileSHA1Sum.SequenceEqual(sha1Sum))
                    {
                        m_InterfaceService.OnCriptoError(fileFinalPath, "SHA1-Sum Error: No coincide!");
                        //File.Delete(fileFinalPath);
                        errors++;
                    }

                    objCryptoStream.Close();
                    Thread.Sleep(1); // No somos codiciosos con el tiempo del procesador
                }

                // Cerramos
                fileCriptopilla.Close();
            }
            catch (Exception ex)
            {
                //m_InterfaceService.OnCriptoError(null, "Error inesperado: " + ex.Message);
                errors++;
            }

            return errors;
        }


        public int Encrypt(ArrayList fileList, string fileDestPath, string key, CriptoFileMethod method, CipherMode cipherMode)
        {
            // Declaramos el Algoritmo Simétrico
            CryptoStream objCryptoStream = null;
            SymmetricAlgorithm symAlg = SymmetricAlgorithm.Create(System.Enum.GetName(typeof(CriptoFileMethod), method));
            if (symAlg == null)
                return -1;

            try
            {
                symAlg.Mode = cipherMode;
            } catch (CryptographicException ex)
            {
                CriptoSystem.ShowMessage(null, ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return -1;
            }

            // Generar Clave y Vector IV
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(key, PASSWORD_SALT);
            byte[] keyGen = pdb.GetBytes(symAlg.KeySize/8);
            byte[] keyIV = pdb.GetBytes(symAlg.BlockSize/8);

            // Escribir Cabecera
            BinaryWriter fileCriptopilla = new BinaryWriter(File.Open(fileDestPath, FileMode.Create));
            fileCriptopilla.Write(FILE_SIGN); // Firma
            fileCriptopilla.Write((byte)1); // Version
            fileCriptopilla.Write((byte)method); // Metodo Encriptacion
            fileCriptopilla.Write((byte)cipherMode); // Modo Cifrado
            fileCriptopilla.Write((UInt32)fileList.Count); // Num Archivos

            // Comenzamos a encriptar y empaquetar los archivos
            int errors = 0;
            for (int iItem = 0; iItem < fileList.Count; iItem++)
            {
                string filePath = (string)fileList[iItem];
                string headFileName = filePath.Substring(filePath.LastIndexOf("\\") + 1);

                try
                {
                    // Nombre del Archivo
                    fileCriptopilla.Write(new System.Text.UTF8Encoding().GetBytes(headFileName));
                    fileCriptopilla.Write((byte)0);

                    // Tamaño del archivo encriptado (de momento 0 porque es desconocido)
                    int fileSizeOffset = (int)fileCriptopilla.BaseStream.Position; // Guardamos la posicion para luego poder cambiar el valor mas facil.
                    UInt32 headFileSize = 0;
                    fileCriptopilla.Write(headFileSize);

                    // Declaramos el descriptor simetrico
                    objCryptoStream = new CryptoStream(fileCriptopilla.BaseStream, symAlg.CreateEncryptor(keyGen, keyIV), CryptoStreamMode.Write);

                    // Abrimos el archivo a encriptar
                    BinaryReader streamRead = new BinaryReader(File.Open(filePath, FileMode.Open));

                    // Reservamos memoria segun disponibilidad
                    UInt32 blockMemorySize = 0;
                    UInt32 freeMemory = (UInt32)(CriptoSystem.GetFreeMemory() * 0.90F);
                    if (streamRead.BaseStream.Length > freeMemory) blockMemorySize = freeMemory;
                    else blockMemorySize = (UInt32)streamRead.BaseStream.Length-1;

                    int iSize = (int)fileCriptopilla.BaseStream.Position; // Guardamos la posicion para calcular despues cuanto ha escrito 'CryptoStream'

                    // Comenzamos a encriptar
                    byte[] tmpData = new byte[blockMemorySize];
                    int readed = 0;
                    do
                    {
                        readed = streamRead.Read(tmpData, 0, tmpData.Length);
                        objCryptoStream.Write(tmpData, 0, readed); // Escribimos en el archivo de destino
                        m_InterfaceService.OnCriptoProgress((byte)method, (UInt32)iItem+1, (UInt32)fileList.Count, headFileName, (UInt32)streamRead.BaseStream.Length, (UInt32)streamRead.BaseStream.Position);
                    } while (readed > 0);

                    // Finalizamos proceso de encriptacion
                    if (!objCryptoStream.HasFlushedFinalBlock) 
                        objCryptoStream.FlushFinalBlock();
                    tmpData = null;

                    headFileSize = (UInt32)(fileCriptopilla.BaseStream.Position - iSize); // Calculamos cuanto ha escrito 'CryptoStream

                    // Guardamos SHA1-Sum
                    streamRead.BaseStream.Position = 0;
                    byte[] fileSHA1Sum = m_SHA1Service.ComputeHash(streamRead.BaseStream);
                    fileCriptopilla.Write((UInt32)fileSHA1Sum.Length);
                    fileCriptopilla.Write(fileSHA1Sum);

                    // Cerramos el archivo
                    streamRead.Close();

                    // Guardamos el tamaño del archivo encriptado (Ahora si lo conocemos)
                    fileCriptopilla.Seek(fileSizeOffset, SeekOrigin.Begin);
                    fileCriptopilla.Write(headFileSize);
                    fileCriptopilla.Seek(0, SeekOrigin.End);

                    Thread.Sleep(1); // No somos codiciosos con el tiempo del procesador
                }
                catch (Exception ex) // ERROR
                {
                    fileCriptopilla.Close();
                    objCryptoStream.Close();

                    File.Delete(fileDestPath);

                    m_InterfaceService.OnCriptoError(headFileName, "Error inesperado: " + ex.Message);

                    errors++; 
                }
            }

            // Cerramos
            objCryptoStream.Close();
            fileCriptopilla.Close();

            return errors;
        }

        public int Encrypt(string filePath, string fileDestPath, string key, CriptoFileMethod method, CipherMode cipherMode)
        {
            ArrayList fileList = new ArrayList();
            fileList.Add(filePath);

            return Encrypt(fileList, fileDestPath, key, method, cipherMode);
        }

        private bool GetHeader(ref BinaryReader pFileStream, ref CriptoFileHeader pHeader)
        {
            pFileStream.BaseStream.Seek(0, SeekOrigin.Begin);

            // Comprobamos que además de la extensión, por dentro es un fichero "Criptopilla"
            byte[] fileSign = new byte[FILE_SIGN.Length];
            fileSign = pFileStream.ReadBytes(FILE_SIGN.Length);
            if (FILE_SIGN.SequenceEqual(fileSign))
            {
                pHeader.m_Version = pFileStream.ReadByte();
                pHeader.m_EncryptMethod = pFileStream.ReadByte();
                pHeader.m_CipherMode = pFileStream.ReadByte();
                pHeader.m_NumFiles = pFileStream.ReadUInt32();
                return true;
            }

            return false; // No es un archivo Criptopilla
        }
    }
}
