﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Criptopilla
{
    class ListViewItemComparer : IComparer
    {
        private int m_Col;

        public ListViewItemComparer()
        {
            m_Col = 0;
        }

        public ListViewItemComparer(int column)
        {
            m_Col = column;
        }

        public int Compare(object x, object y)
        {
            int returnVal = -1;
            returnVal = String.Compare(((ListViewItem)x).SubItems[m_Col].Text, ((ListViewItem)y).SubItems[m_Col].Text);
            return returnVal;
        }
    }
}
