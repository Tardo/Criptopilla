﻿/*
    Criptopilla 2014 - http://www.lospillaos.es
 
    En este archivo han contribuido:
      - WhiteSkull
      - unsigned char*
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Criptopilla
{

    /// <summary>
    /// Esta clase abre una ventana donde visualizar el contenido de un archivo
    /// </summary>
    public partial class FileViewerWindow : Form
    {
        private string          m_FilePath;
        private delegate void   delegateLoadFile();

        public FileViewerWindow(string _fichero)
        {
            InitializeComponent();
            VerBytes.Visible =VerBytesDec.Visible=
            VerASCII.Visible = false;
            this.Text += " - " + _fichero;
            OnSizeChangedFileViewerWindow(this, new EventArgs());

            VerBytes.Columns.Add("filas","filas");
            VerBytes.Columns[0].ReadOnly = true;
            VerBytes.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            VerBytes.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            VerBytes.Columns[0].DefaultCellStyle.BackColor = Color.BlueViolet;

            VerBytesDec.Columns.Add("filas", "filas");
            VerBytesDec.Columns[0].ReadOnly = true;
            VerBytesDec.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            VerBytesDec.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            VerBytesDec.Columns[0].DefaultCellStyle.BackColor = Color.BlueViolet;

            for (int contador = 0; contador < 256; contador++)
            {
                VerBytes.Columns.Add(String.Format("{0:X}", contador), String.Format("{0:X}", contador));
                VerBytes.Columns[VerBytes.Columns.Count -1].ReadOnly = true;
                VerBytes.Columns[VerBytes.Columns.Count -1].SortMode = DataGridViewColumnSortMode.NotSortable;
                VerBytes.Columns[VerBytes.Columns.Count -1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                VerBytesDec.Columns.Add(contador.ToString(), contador.ToString());
                VerBytesDec.Columns[VerBytesDec.Columns.Count - 1].ReadOnly = true;
                VerBytesDec.Columns[VerBytesDec.Columns.Count - 1].SortMode = DataGridViewColumnSortMode.NotSortable;
                VerBytesDec.Columns[VerBytesDec.Columns.Count - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            }            

            
            m_FilePath = _fichero;
            //new Thread(new ThreadStart(delegate()
            //{
            //    this.Invoke(new cargaDelegada(CargaBytes)); 
            //})).Start();
        }

        private void LoadFileBytes()
        {
            string[] celdasHex = new string[257],celdasDec=new string[257];

            if (VerBytes.Rows.Count < 1)
            {
                Int16 fila = 0, contador = 1;

                VerBytes.Rows.Add(1 + (int)new FileInfo(m_FilePath).Length / 256);
                VerBytes.Rows[fila].Cells[0].Value = 0;

                VerBytesDec.Rows.Add(1 + (int)new FileInfo(m_FilePath).Length / 256);
                VerBytesDec.Rows[fila].Cells[0].Value = 0;

                celdasDec[0]=celdasHex[0] = "0";
                foreach (byte byt in File.ReadAllBytes(m_FilePath))
                    if (contador > 255)
                    {
                        contador = 0;
                        celdasDec[contador]=celdasHex[contador++] = fila.ToString();
                        VerBytes.Rows[fila].SetValues(celdasHex);
                        VerBytesDec.Rows[fila++].SetValues(celdasDec);
                        VerBytes.Refresh();
                        VerBytesDec.Refresh();
                        celdasDec = new string[257];
                        celdasHex = new string[257];
                    }
                    else
                    {
                        celdasHex[contador] = String.Format("{0:X}", byt);
                        celdasDec[contador++] = byt.ToString();
                    }
                celdasDec[0]=celdasHex[0] = fila.ToString();
                VerBytes.Rows[fila].SetValues(celdasHex);
                VerBytesDec.Rows[fila].SetValues(celdasDec);
            }
        }

        private void LoadFileASCII()
        {
            VerASCII.Text=File.ReadAllText(m_FilePath, Encoding.ASCII);
            etiquetaInformativa.Text = new FileInfo(m_FilePath).Length.ToString("N0")+" bytes";
        }

        private void OnClickRadioButtonOption(object sender, EventArgs e)
        {
            Auditor.printMessage(" Comprobando "+((RadioButton)sender).Text);
            
            switch (((RadioButton)sender).Text)
            {
                case ("HEXADECIMAL"):
                    VerBytesDec.Visible = false;
                    VerBytes.Visible = true;
                    VerASCII.Visible = false;
                    break;
                case ("DECIMAL"):
                    VerBytesDec.Visible = true;
                    VerBytes.Visible = false;
                    VerASCII.Visible = false;
                    break;
                default:
                    if (VerASCII.Text.Length<1) LoadFileASCII();
                    VerBytesDec.Visible=VerBytes.Visible = false;
                    VerASCII.Visible = true;
                    break;
            }
        }

        private void OnSizeChangedFileViewerWindow(object sender, EventArgs e)
        {
            OpcionVerHex.Location = new Point((this.Width / 2)-(OpcionVerHex.Size.Width/2)-18, OpcionVerHex.Location.Y);
            OpcionVerDEC.Location = new Point(this.Width-100, OpcionVerDEC.Location.Y);

            VerBytesDec.Size=VerBytes.Size= VerASCII.Size = new Size(this.Width - 14, this.Height - 85);
                        
        }
    }
}
