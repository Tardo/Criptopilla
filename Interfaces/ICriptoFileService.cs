﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Criptopilla
{
    interface ICriptoFileService
    {
        void OnCriptoError(string fileName, string errorMsg);
        void OnCriptoProgress(byte method, UInt32 itemIndex, UInt32 totalItems, string fileName, UInt32 totalBytes, UInt32 recvBytes);
    }
}
