﻿namespace Criptopilla
{
    partial class EncryptDialog
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.ButtonOk = new System.Windows.Forms.Button();
            this.textBoxInputKey = new System.Windows.Forms.TextBox();
            this.etiquetaDeClaveEmpaquetado = new System.Windows.Forms.Label();
            this.textBoxInputKeyConfirm = new System.Windows.Forms.TextBox();
            this.etiquetaDeConfirmarProgreso = new System.Windows.Forms.Label();
            this.opcionMostrar = new System.Windows.Forms.CheckBox();
            this.opcionEliminar = new System.Windows.Forms.CheckBox();
            this.opcionEmpaquetado = new System.Windows.Forms.CheckBox();
            this.panelDeClave = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxEncryptMethodCipher = new System.Windows.Forms.ComboBox();
            this.comboBoxEncryptMethod = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelInfoEncrypt = new System.Windows.Forms.Label();
            this.labelInfoPack = new System.Windows.Forms.Label();
            this.progressBarEncrypt = new System.Windows.Forms.ProgressBar();
            this.progressBarPack = new System.Windows.Forms.ProgressBar();
            this.ButtonModeBackground = new System.Windows.Forms.Button();
            this.buttonPause = new System.Windows.Forms.Button();
            this.panelDeBotones = new System.Windows.Forms.FlowLayoutPanel();
            this.panelDeOpciones = new System.Windows.Forms.FlowLayoutPanel();
            this.labelEstimedTime = new System.Windows.Forms.Label();
            this.labelLapsedTime = new System.Windows.Forms.Label();
            this.panelDeClave.SuspendLayout();
            this.panelDeBotones.SuspendLayout();
            this.panelDeOpciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.AutoSize = true;
            this.buttonCancel.Location = new System.Drawing.Point(5, 44);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(130, 35);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Cancelar";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.OnClickButtonCancel);
            // 
            // ButtonOk
            // 
            this.ButtonOk.AutoSize = true;
            this.ButtonOk.Location = new System.Drawing.Point(141, 44);
            this.ButtonOk.Name = "ButtonOk";
            this.ButtonOk.Size = new System.Drawing.Size(130, 35);
            this.ButtonOk.TabIndex = 8;
            this.ButtonOk.Text = "Aceptar";
            this.ButtonOk.UseVisualStyleBackColor = true;
            this.ButtonOk.Click += new System.EventHandler(this.OnClickButtonAccept);
            // 
            // textBoxInputKey
            // 
            this.textBoxInputKey.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.textBoxInputKey.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInputKey.Location = new System.Drawing.Point(7, 31);
            this.textBoxInputKey.MaxLength = 32;
            this.textBoxInputKey.Name = "textBoxInputKey";
            this.textBoxInputKey.Size = new System.Drawing.Size(261, 23);
            this.textBoxInputKey.TabIndex = 0;
            this.textBoxInputKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInputKey.UseSystemPasswordChar = true;
            this.textBoxInputKey.UseWaitCursor = true;
            // 
            // etiquetaDeClaveEmpaquetado
            // 
            this.etiquetaDeClaveEmpaquetado.AutoSize = true;
            this.etiquetaDeClaveEmpaquetado.Location = new System.Drawing.Point(9, 16);
            this.etiquetaDeClaveEmpaquetado.Name = "etiquetaDeClaveEmpaquetado";
            this.etiquetaDeClaveEmpaquetado.Size = new System.Drawing.Size(116, 13);
            this.etiquetaDeClaveEmpaquetado.TabIndex = 2;
            this.etiquetaDeClaveEmpaquetado.Text = "Introduce clave segura";
            // 
            // textBoxInputKeyConfirm
            // 
            this.textBoxInputKeyConfirm.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInputKeyConfirm.Location = new System.Drawing.Point(7, 76);
            this.textBoxInputKeyConfirm.MaxLength = 32;
            this.textBoxInputKeyConfirm.Name = "textBoxInputKeyConfirm";
            this.textBoxInputKeyConfirm.Size = new System.Drawing.Size(261, 23);
            this.textBoxInputKeyConfirm.TabIndex = 2;
            this.textBoxInputKeyConfirm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInputKeyConfirm.UseSystemPasswordChar = true;
            this.textBoxInputKeyConfirm.UseWaitCursor = true;
            // 
            // etiquetaDeConfirmarProgreso
            // 
            this.etiquetaDeConfirmarProgreso.AutoSize = true;
            this.etiquetaDeConfirmarProgreso.Location = new System.Drawing.Point(9, 60);
            this.etiquetaDeConfirmarProgreso.Name = "etiquetaDeConfirmarProgreso";
            this.etiquetaDeConfirmarProgreso.Size = new System.Drawing.Size(80, 13);
            this.etiquetaDeConfirmarProgreso.TabIndex = 2;
            this.etiquetaDeConfirmarProgreso.Text = "Confirmar clave";
            // 
            // opcionMostrar
            // 
            this.opcionMostrar.AutoSize = true;
            this.opcionMostrar.Location = new System.Drawing.Point(8, 69);
            this.opcionMostrar.Name = "opcionMostrar";
            this.opcionMostrar.Size = new System.Drawing.Size(90, 17);
            this.opcionMostrar.TabIndex = 3;
            this.opcionMostrar.Text = "Mostrar clave";
            this.opcionMostrar.UseVisualStyleBackColor = true;
            this.opcionMostrar.CheckedChanged += new System.EventHandler(this.OnClickButtonOptions);
            // 
            // opcionEliminar
            // 
            this.opcionEliminar.AutoSize = true;
            this.opcionEliminar.Location = new System.Drawing.Point(153, 46);
            this.opcionEliminar.Name = "opcionEliminar";
            this.opcionEliminar.Size = new System.Drawing.Size(100, 17);
            this.opcionEliminar.TabIndex = 4;
            this.opcionEliminar.Text = "Eliminar fuentes";
            this.opcionEliminar.UseVisualStyleBackColor = true;
            // 
            // opcionEmpaquetado
            // 
            this.opcionEmpaquetado.AutoSize = true;
            this.opcionEmpaquetado.Location = new System.Drawing.Point(8, 46);
            this.opcionEmpaquetado.Name = "opcionEmpaquetado";
            this.opcionEmpaquetado.Size = new System.Drawing.Size(139, 17);
            this.opcionEmpaquetado.TabIndex = 5;
            this.opcionEmpaquetado.Text = "Empaquetado individual";
            this.opcionEmpaquetado.UseVisualStyleBackColor = true;
            // 
            // panelDeClave
            // 
            this.panelDeClave.Controls.Add(this.label2);
            this.panelDeClave.Controls.Add(this.comboBoxEncryptMethodCipher);
            this.panelDeClave.Controls.Add(this.comboBoxEncryptMethod);
            this.panelDeClave.Controls.Add(this.label1);
            this.panelDeClave.Controls.Add(this.labelInfoEncrypt);
            this.panelDeClave.Controls.Add(this.labelInfoPack);
            this.panelDeClave.Controls.Add(this.progressBarEncrypt);
            this.panelDeClave.Controls.Add(this.progressBarPack);
            this.panelDeClave.Controls.Add(this.textBoxInputKeyConfirm);
            this.panelDeClave.Controls.Add(this.textBoxInputKey);
            this.panelDeClave.Controls.Add(this.etiquetaDeClaveEmpaquetado);
            this.panelDeClave.Controls.Add(this.etiquetaDeConfirmarProgreso);
            this.panelDeClave.Location = new System.Drawing.Point(12, 8);
            this.panelDeClave.Name = "panelDeClave";
            this.panelDeClave.Size = new System.Drawing.Size(274, 149);
            this.panelDeClave.TabIndex = 6;
            this.panelDeClave.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(103, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Cifrado";
            // 
            // comboBoxEncryptMethodCipher
            // 
            this.comboBoxEncryptMethodCipher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEncryptMethodCipher.Location = new System.Drawing.Point(100, 118);
            this.comboBoxEncryptMethodCipher.Name = "comboBoxEncryptMethodCipher";
            this.comboBoxEncryptMethodCipher.Size = new System.Drawing.Size(168, 21);
            this.comboBoxEncryptMethodCipher.TabIndex = 4;
            // 
            // comboBoxEncryptMethod
            // 
            this.comboBoxEncryptMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEncryptMethod.FormattingEnabled = true;
            this.comboBoxEncryptMethod.Location = new System.Drawing.Point(7, 118);
            this.comboBoxEncryptMethod.Name = "comboBoxEncryptMethod";
            this.comboBoxEncryptMethod.Size = new System.Drawing.Size(87, 21);
            this.comboBoxEncryptMethod.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Encriptación";
            // 
            // labelInfoEncrypt
            // 
            this.labelInfoEncrypt.Location = new System.Drawing.Point(233, 78);
            this.labelInfoEncrypt.Name = "labelInfoEncrypt";
            this.labelInfoEncrypt.Size = new System.Drawing.Size(35, 17);
            this.labelInfoEncrypt.TabIndex = 6;
            this.labelInfoEncrypt.Text = "0%";
            this.labelInfoEncrypt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelInfoPack
            // 
            this.labelInfoPack.Location = new System.Drawing.Point(233, 33);
            this.labelInfoPack.Name = "labelInfoPack";
            this.labelInfoPack.Size = new System.Drawing.Size(35, 16);
            this.labelInfoPack.TabIndex = 5;
            this.labelInfoPack.Text = "0%";
            this.labelInfoPack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBarEncrypt
            // 
            this.progressBarEncrypt.Location = new System.Drawing.Point(7, 78);
            this.progressBarEncrypt.Name = "progressBarEncrypt";
            this.progressBarEncrypt.Size = new System.Drawing.Size(224, 19);
            this.progressBarEncrypt.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarEncrypt.TabIndex = 4;
            // 
            // progressBarPack
            // 
            this.progressBarPack.Location = new System.Drawing.Point(7, 33);
            this.progressBarPack.Name = "progressBarPack";
            this.progressBarPack.Size = new System.Drawing.Size(224, 19);
            this.progressBarPack.TabIndex = 3;
            // 
            // ButtonModeBackground
            // 
            this.ButtonModeBackground.AutoSize = true;
            this.ButtonModeBackground.Location = new System.Drawing.Point(5, 3);
            this.ButtonModeBackground.Name = "ButtonModeBackground";
            this.ButtonModeBackground.Size = new System.Drawing.Size(130, 35);
            this.ButtonModeBackground.TabIndex = 5;
            this.ButtonModeBackground.Text = "Background";
            this.ButtonModeBackground.UseVisualStyleBackColor = true;
            // 
            // buttonPause
            // 
            this.buttonPause.AutoSize = true;
            this.buttonPause.Location = new System.Drawing.Point(141, 3);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(130, 35);
            this.buttonPause.TabIndex = 6;
            this.buttonPause.Text = "Pausar";
            this.buttonPause.UseVisualStyleBackColor = true;
            // 
            // panelDeBotones
            // 
            this.panelDeBotones.Controls.Add(this.buttonPause);
            this.panelDeBotones.Controls.Add(this.ButtonModeBackground);
            this.panelDeBotones.Controls.Add(this.ButtonOk);
            this.panelDeBotones.Controls.Add(this.buttonCancel);
            this.panelDeBotones.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.panelDeBotones.Location = new System.Drawing.Point(12, 211);
            this.panelDeBotones.Margin = new System.Windows.Forms.Padding(1);
            this.panelDeBotones.Name = "panelDeBotones";
            this.panelDeBotones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panelDeBotones.Size = new System.Drawing.Size(274, 84);
            this.panelDeBotones.TabIndex = 1;
            // 
            // panelDeOpciones
            // 
            this.panelDeOpciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDeOpciones.Controls.Add(this.labelEstimedTime);
            this.panelDeOpciones.Controls.Add(this.labelLapsedTime);
            this.panelDeOpciones.Controls.Add(this.opcionEmpaquetado);
            this.panelDeOpciones.Controls.Add(this.opcionEliminar);
            this.panelDeOpciones.Controls.Add(this.opcionMostrar);
            this.panelDeOpciones.Location = new System.Drawing.Point(12, 162);
            this.panelDeOpciones.Margin = new System.Windows.Forms.Padding(1);
            this.panelDeOpciones.Name = "panelDeOpciones";
            this.panelDeOpciones.Padding = new System.Windows.Forms.Padding(5);
            this.panelDeOpciones.Size = new System.Drawing.Size(274, 47);
            this.panelDeOpciones.TabIndex = 7;
            // 
            // labelEstimedTime
            // 
            this.labelEstimedTime.Location = new System.Drawing.Point(8, 5);
            this.labelEstimedTime.Name = "labelEstimedTime";
            this.labelEstimedTime.Size = new System.Drawing.Size(259, 18);
            this.labelEstimedTime.TabIndex = 6;
            this.labelEstimedTime.Text = "Tiempo estimado:";
            this.labelEstimedTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLapsedTime
            // 
            this.labelLapsedTime.Location = new System.Drawing.Point(8, 23);
            this.labelLapsedTime.Name = "labelLapsedTime";
            this.labelLapsedTime.Size = new System.Drawing.Size(259, 20);
            this.labelLapsedTime.TabIndex = 7;
            this.labelLapsedTime.Text = "Tiempo transcurrido:";
            this.labelLapsedTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EncryptDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 308);
            this.Controls.Add(this.panelDeOpciones);
            this.Controls.Add(this.panelDeBotones);
            this.Controls.Add(this.panelDeClave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EncryptDialog";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Encriptacion en proceso...";
            this.panelDeClave.ResumeLayout(false);
            this.panelDeClave.PerformLayout();
            this.panelDeBotones.ResumeLayout(false);
            this.panelDeBotones.PerformLayout();
            this.panelDeOpciones.ResumeLayout(false);
            this.panelDeOpciones.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button ButtonOk;
        private System.Windows.Forms.TextBox textBoxInputKey;
        private System.Windows.Forms.Label etiquetaDeClaveEmpaquetado;
        private System.Windows.Forms.TextBox textBoxInputKeyConfirm;
        private System.Windows.Forms.Label etiquetaDeConfirmarProgreso;
        private System.Windows.Forms.CheckBox opcionMostrar;
        private System.Windows.Forms.CheckBox opcionEliminar;
        private System.Windows.Forms.CheckBox opcionEmpaquetado;
        private System.Windows.Forms.GroupBox panelDeClave;
        private System.Windows.Forms.Button ButtonModeBackground;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.FlowLayoutPanel panelDeBotones;
        private System.Windows.Forms.FlowLayoutPanel panelDeOpciones;
        private System.Windows.Forms.ProgressBar progressBarEncrypt;
        private System.Windows.Forms.ProgressBar progressBarPack;
        private System.Windows.Forms.Label labelInfoEncrypt;
        private System.Windows.Forms.Label labelInfoPack;
        private System.Windows.Forms.Label labelEstimedTime;
        private System.Windows.Forms.Label labelLapsedTime;
        private System.Windows.Forms.ComboBox comboBoxEncryptMethod;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxEncryptMethodCipher;
        private System.Windows.Forms.Label label2;

    }
}
