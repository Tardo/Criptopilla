﻿/*
    Criptopilla 2014 - http://www.lospillaos.es
 
    En este archivo han contribuido:
      - WhiteSkull
      - unsigned char*
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Criptopilla
{
    partial class EncryptDialog : Form, ICriptoFileService
    {
        delegate void delegateCloseDialog();
        delegate void delegateProcessDecryptFinished(bool errors);
        delegate void delegateOnCriptoProgress(byte method, UInt32 itemIndex, UInt32 totalItems, string fileName, UInt32 totalBytes, UInt32 recvBytes);
        delegate void delegateShowMessage(IWin32Window win, string msg, string title, MessageBoxButtons buttons, MessageBoxIcon icon);

        enum EncryptDialogMode
        {
            InputPasswordEncrypt = 0,
            InputPasswordDecrypt = 1,
            EncryptProcessing = 2,
            DecryptProcessing = 3,
        }

        CriptoFileServiceProvider   m_CriptoFileService;            // Usadado para los procesos de encriptacion y empaquetado
        string                      m_CurrentPath = "";             // Nos indica la ruta seleccionada en el 'FileFolderBrowser'
        ArrayList                   m_vThreads = new ArrayList();   // Lista con los hilos
        ArrayList                   m_vSelectedFiles;               // Lista con los elementos seleccionados en el 'FileFolderBrowser'
        EncryptDialogMode           m_DialogMode;                   // Indica si el dialog esta en modo encriptar o desencriptar

        // ENCRIPTAR
        public EncryptDialog(ref ArrayList pFilesList , string currentPath)
        {
            m_DialogMode = EncryptDialogMode.InputPasswordEncrypt;
            m_CurrentPath = currentPath;
            m_vSelectedFiles = (ArrayList)pFilesList.Clone();
            m_vSelectedFiles = CriptoSystem.GenerateRecursiveFileList(m_vSelectedFiles);

            // Inicializar Componentes
            InitializeComponent();
            InitializeEncryptDialog();
            
            // En base al numero de ficheros seleccionados permite empaquetar o no
            if (m_vSelectedFiles.Count == 1) 
                opcionEmpaquetado.Visible = false;
            
            // Ocultamos la siguiente fase
            ShowCriptoFileServiceProgress(false);

            m_CriptoFileService = new CriptoFileServiceProvider((ICriptoFileService)this);
        }


        // DESENCRIPTAR
        public EncryptDialog()
        {
            m_DialogMode = EncryptDialogMode.InputPasswordDecrypt;

            // Inicializar Componentes
            InitializeComponent();
            InitializeEncryptDialog();

            textBoxInputKey.Visible =
            textBoxInputKeyConfirm.Visible =
            opcionEliminar.Visible =
            opcionMostrar.Visible = true;

            opcionEmpaquetado.Visible =
            buttonPause.Visible =
            ButtonModeBackground.Visible =
            progressBarPack.Visible =
            labelInfoPack.Visible =
            progressBarEncrypt.Visible =
            labelInfoEncrypt.Visible =
            labelEstimedTime.Visible =
            labelLapsedTime.Visible = false;

            m_CriptoFileService = new CriptoFileServiceProvider((ICriptoFileService)this);
        }

        private void SetDialogMode(EncryptDialogMode mode)
        {

        }

        private void InitializeEncryptDialog()
        {
            // Ponemos ToolTip al cajetin de la contraseña
            (new ToolTip()).SetToolTip(textBoxInputKey, "Recuerde que la clave debe tener una longitud mayor o igual\n a 8 caracteres, en los cuales no pueden figurar &/%().");

            // Rellenamos la lista con los posibles metodos de encriptación
            foreach (string enumName in Enum.GetNames(typeof(CriptoFileMethod)))
                comboBoxEncryptMethod.Items.Add(enumName);

            // Rellenamos la lista con los posibles modos de cifrado
            comboBoxEncryptMethodCipher.Items.Add("Cipher Block Chaining (CBC)");
            comboBoxEncryptMethodCipher.Items.Add("Cipher Feedback (CFB)");
            comboBoxEncryptMethodCipher.Items.Add("Cipher Text Stealing CTS");
            comboBoxEncryptMethodCipher.Items.Add("Electronic Codebook (ECB)");
            comboBoxEncryptMethodCipher.Items.Add("Output Feedback (OFB)");

            // Cambios concretos para el modo desencriptación
            if (m_DialogMode == EncryptDialogMode.InputPasswordDecrypt)
            {
                this.Text = "Desencriptar"; // Cambiamos el titulo del dialogo, que por defecto es "Encriptar"
                comboBoxEncryptMethod.Enabled = false;  // En este modo no hace falta este parámetro
                comboBoxEncryptMethodCipher.Enabled = false;
            }
            else
            {
                comboBoxEncryptMethod.SelectedIndex = 0;
                comboBoxEncryptMethodCipher.SelectedIndex = 1;
            }
        }


        private void ShowCriptoFileServiceProgress(bool status)
        {
            progressBarPack.Visible =
            labelInfoPack.Visible =
            progressBarEncrypt.Visible =
            labelInfoEncrypt.Visible =
            labelEstimedTime.Visible =
            labelLapsedTime.Visible =
            buttonPause.Visible =
            ButtonModeBackground.Visible = status;
        }

        // Opciones del dialogo
        private void OnClickButtonOptions(object sender, EventArgs e)
        {
            textBoxInputKey.UseSystemPasswordChar =
            textBoxInputKeyConfirm.UseSystemPasswordChar = opcionMostrar.Checked ^ true;
        }

        // Comprueba que la clave se ajusta a los mínimos de seguridad
        private bool ValidateEncryptKey()
        {
            if (textBoxInputKey.Text.Length >= 8)
            {
                if (textBoxInputKey.Text == textBoxInputKeyConfirm.Text)
                {
                    // TODO: Comprobar que la clave no contenga caracteres extraños
                    return true;
                }
            }

            String errorMsg = "";
            if (textBoxInputKey.Text != textBoxInputKeyConfirm.Text) errorMsg = "Por favor, confirme la clave.";
            else if (textBoxInputKey.Text.Length < 8) errorMsg = "La clave no es lo suficientemente grande,\n debe introducir más de 8 caracteres.";
            else errorMsg = "Es posible que la clave contenga caracteres\n no válidos, por favor revisela y vuelva a introducir \nla clave.";
           
            if (errorMsg.Length > 0)
                CriptoSystem.ShowMessage(this, errorMsg, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            return false;
        }

        // Comienza el proceso de DESENCRIPTACION
        private void ComienzaElProcesoDeDesincriptacion(object sender, EventArgs e)
        {
            if (ValidateEncryptKey())
            {
                // Indicamos donde se alojaran los ficheros desencriptados...
                FolderBrowserDialog selectFolderDialog = new FolderBrowserDialog();
                selectFolderDialog.SelectedPath = m_vSelectedFiles[0].ToString().Substring(0, m_vSelectedFiles[0].ToString().LastIndexOf("\\"));
                selectFolderDialog.Description = "Elige el lugar o directorio donde se guardarán el o los elementos desempaquetados.";

                if (selectFolderDialog.ShowDialog() == DialogResult.OK)
                {   // Cambiamos el diseño dinamicamente del dialogo.                 
                    etiquetaDeClaveEmpaquetado.Text = "Desempaquetando...";
                    etiquetaDeConfirmarProgreso.Text = "Progreso";

                    textBoxInputKey.Visible =
                    textBoxInputKeyConfirm.Visible =
                    opcionEliminar.Visible =
                    opcionMostrar.Visible =
                    ButtonOk.Visible = false;

                    opcionEmpaquetado.Visible =
                    buttonPause.Visible =
                    ButtonModeBackground.Visible =
                    progressBarPack.Visible =
                    labelInfoPack.Visible =
                    progressBarEncrypt.Visible =
                    labelInfoEncrypt.Visible =
                    labelEstimedTime.Visible =
                    labelLapsedTime.Visible = true;


                    Thread threadDecrypt;
                    m_vThreads.Clear();

                    // Desencriptamos los archivos seleccionados en el 'FileFolderBrowser'
                    for (int i = 0; i < m_vSelectedFiles.Count; i++)
                    {
                        string filePath = (string)m_vSelectedFiles[i];
                        threadDecrypt = new Thread(new ThreadStart(delegate {
                            bool errors = (m_CriptoFileService.Decrypt(filePath, selectFolderDialog.SelectedPath + "\\", textBoxInputKey.Text) != 0);
                            Invoke(new delegateProcessDecryptFinished(ProcessDecryptFinished), new object[]{ errors });
                        }));
                        m_vThreads.Add(threadDecrypt);
                        threadDecrypt.Start();
                    }
                }
            }           
        }

        private void CloseDialog()
        {
            this.Close();
        }

        private void ProcessDecryptFinished(bool errors)
        {
            if (errors)
                CriptoSystem.ShowMessage(this, "El proceso de desencriptado y desempaquetado ha finalizado con errores!\nArchivo corrupto o contraseña incorrecta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                CriptoSystem.ShowMessage(this, "Archivo desencriptado y desempaquetado correctamente :)", "Proceso Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                CloseDialog();
            }
        }

        // BOTON QUE "ACEPTA" EL COMIENZO DE LA ENCRIPTACION 
        private void OnClickButtonAccept(object sender, EventArgs e)
        {
            if (ValidateEncryptKey()) // Primero debe cumplirse las condiciones mínimas de seguridad para la clave
            {                
                labelEstimedTime.Text = "Tiempo estimado: 00:00:00"; // Para el tiempo de estimacion <- NO FUNCIONA AUN

                // En caso de que se haya seleccionado un solo fichero o la opcion de Empaquetado este habilitado
                if (opcionEmpaquetado.Checked || m_vSelectedFiles.Count == 1)
                {
                    // El usuario debe indicar donde alojará los ficheros desencriptados
                    FolderBrowserDialog dialogoDeNavegacion = new FolderBrowserDialog();
                    dialogoDeNavegacion.SelectedPath = m_vSelectedFiles[0].ToString().Substring(0, m_vSelectedFiles[0].ToString().LastIndexOf("\\"));
                    dialogoDeNavegacion.Description = "Elige el lugar o directorio donde se guardarán el o los elementos encriptados.";
                    // Una vez elegido el directorio destino, comienzan la encriptacion 
                    // con "Encriptar" donde arrancan los hilos que crean el fichero

                    string fileName = (string)m_vSelectedFiles[0];
                    fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                    fileName = fileName.Substring(0, fileName.LastIndexOf(".")) + ".crip";
                    if (dialogoDeNavegacion.ShowDialog() == DialogResult.OK) ComienzaElProcesoDeEncriptacion(dialogoDeNavegacion.SelectedPath + "\\" + fileName);
                } // En caso contrario, que la opcion de empaquetado no esté habilitada O la lista de ficheros seleccionados 
                else // para encriptar, sea mayor de uno... entonces se procederá al empaquetado individual deficheros
                {
                    // Se debe indicar el lugar y nombre del fichero
                    SaveFileDialog dialogoDeFichero = new SaveFileDialog();
                    dialogoDeFichero.CheckPathExists = true;
                    dialogoDeFichero.OverwritePrompt = true;
                    dialogoDeFichero.Filter = "Ficheros Criptopilla (*.crip)|*.crip";
                    dialogoDeFichero.InitialDirectory = m_vSelectedFiles[0].ToString().Substring(0, m_vSelectedFiles[0].ToString().LastIndexOf("\\"));
                    dialogoDeFichero.Title = "Seleccione lugar o directorio e introduzca nombre del paquete encriptado";
                    dialogoDeFichero.SupportMultiDottedExtensions = true;
                    dialogoDeFichero.AddExtension = true;

                    // Una vez indicado el nombre y lugar, comienza la encriptacion de todos los ficheros seleccionados
                    if (dialogoDeFichero.ShowDialog() == DialogResult.OK)
                    {
                        string fileName = dialogoDeFichero.FileName;
                        fileName = fileName.Substring(0, fileName.LastIndexOf(".")) + ".crip";
                        ComienzaElProcesoDeEncriptacion(dialogoDeFichero.FileName);
                    }
                }
            }
        } 

        
        // Método que es invocado desde la aplicación para desencriptar, ya que POR DEFECTO
        // esta diseñada para ENCRIPTAR.
        public void ChangeToDecrypMode(ref ArrayList elementos_)
        {
            // Crea un clon de los ficheros seleccionados (ni me acuerdo para que)
            m_vSelectedFiles = (ArrayList)elementos_.Clone();
            // Quita la funcion asociada por defecto al boton aceptar, que es la ENCRIPTAR
            this.ButtonOk.Click -= new EventHandler(this.OnClickButtonAccept);
            // y reasocia la nueva funcion para DESENCRIPTAR
            this.ButtonOk.Click += new System.EventHandler(this.ComienzaElProcesoDeDesincriptacion);
            // Muestra el dialogo de DESENCRIPTACION
            ShowDialog();
        } // fin >Método del método invocado en la aplicación para DESENCRIPTAR<

        // Comienza el proceso de ENCRIPTACION
        private void ComienzaElProcesoDeEncriptacion(string nombreOruta) 
        {         
            this.Text = "Encriptando...";

            etiquetaDeClaveEmpaquetado.Text = "Empaquetando...";
            etiquetaDeConfirmarProgreso.Text = "Progreso";
            textBoxInputKey.Visible =
            textBoxInputKeyConfirm.Visible =
            opcionEliminar.Visible =
            opcionEmpaquetado.Visible =
            opcionMostrar.Visible =
            ButtonOk.Visible = false;

            buttonPause.Visible=
            ButtonModeBackground.Visible=
            progressBarPack.Visible =
            labelInfoPack.Visible =
            progressBarEncrypt.Visible =
            labelInfoEncrypt.Visible =
            labelEstimedTime.Visible =
            labelLapsedTime.Visible = true;

            Thread hilo;
            m_vThreads.Clear();

            CriptoFileMethod encryptMethod = (CriptoFileMethod)comboBoxEncryptMethod.SelectedIndex;
            CipherMode cipherMode = (CipherMode)comboBoxEncryptMethodCipher.SelectedIndex;

            // Comprobamos si se desea empaquetar individualmente los ficheros
            if (opcionEmpaquetado.Checked) 
            {    // creamos paquetes individuales por cada fichero
                hilo = new Thread(new ThreadStart(delegate {
                    int errors = DoEncryption(m_CurrentPath, textBoxInputKey.Text, encryptMethod, cipherMode);
                    if (errors != 0)
                    {
                        if (m_DialogMode == EncryptDialogMode.InputPasswordEncrypt)
                            Invoke(new delegateShowMessage(CriptoSystem.ShowMessage), new object[] { this, "Ocurrieron errores!", "Fallo al Encriptar!", MessageBoxButtons.OK, MessageBoxIcon.Error });
                        else
                            Invoke(new delegateShowMessage(CriptoSystem.ShowMessage), new object[] { this, "Ocurrieron errores! ¿Clave incorrecta o archivo dañado?", "Fallo al Desencriptar!", MessageBoxButtons.OK, MessageBoxIcon.Error });
                    } else
                        Invoke(new delegateCloseDialog(CloseDialog));
                }));
                m_vThreads.Add(hilo);
                hilo.Start();
            }
            else // Lo guardamos todo en un paquete global!!!
            {
                hilo = new Thread(new ThreadStart(delegate {
                    int errors = m_CriptoFileService.Encrypt(m_vSelectedFiles, nombreOruta, textBoxInputKey.Text, encryptMethod, cipherMode);
                    if (errors > 0)
                    {
                        if (m_DialogMode == EncryptDialogMode.InputPasswordEncrypt)
                            Invoke(new delegateShowMessage(CriptoSystem.ShowMessage), new object[] { this, "Ocurrieron errores!", "Fallo al Encriptar!", MessageBoxButtons.OK, MessageBoxIcon.Error });
                        else
                            Invoke(new delegateShowMessage(CriptoSystem.ShowMessage), new object[] { this, "Ocurrieron errores! ¿Clave incorrecta o archivo dañado?", "Fallo al Desencriptar!", MessageBoxButtons.OK, MessageBoxIcon.Error });
                    }
                }));
                m_vThreads.Add(hilo);
                hilo.Start();
            }
            
            // comprobamos si hay eliminacion segura
            if (opcionEliminar.Checked)
            {
                for (int iElemento = 0; iElemento < m_vSelectedFiles.Count; iElemento++)
                {
                    if (new FileInfo((string)m_vSelectedFiles[iElemento]).Exists)
                        File.Delete((string)m_vSelectedFiles[iElemento]);
                }
            }
        }


        private int DoEncryption(string pathTo, string key, CriptoFileMethod method, CipherMode cipherMode)
        {
            int errors = 0;
            for (int i = 0; i < m_vSelectedFiles.Count; i++)
                errors += m_CriptoFileService.Encrypt((string)m_vSelectedFiles[i], pathTo, key, method, cipherMode);

            return errors;
        }



        /////////////////////////////////
        // EVENTOS CRIPTOFILESERVIVE
        ////////////////////////////////////////

        void ICriptoFileService.OnCriptoError(string fileName, string msgError)
        {

        }

        void ICriptoFileService.OnCriptoProgress(byte method, UInt32 itemIndex, UInt32 totalItems, string fileName, UInt32 totalBytes, UInt32 recvBytes)
        {
            Invoke(new delegateOnCriptoProgress(OnCriptoprogressDelegated)
                                , new object[] { method, itemIndex, totalItems, fileName, totalBytes, recvBytes });
        }

        void OnCriptoprogressDelegated(byte method, UInt32 itemIndex, UInt32 totalItems, string fileName, UInt32 totalBytes, UInt32 recvBytes)
        {
            progressBarPack.Maximum = (int)totalItems;
            progressBarPack.Value = (int)itemIndex;
            labelInfoPack.Text = itemIndex + "/" + totalItems;
            if (m_DialogMode == EncryptDialogMode.InputPasswordEncrypt) etiquetaDeClaveEmpaquetado.Text = "Empaquetando '" + fileName + "'...";
            else etiquetaDeClaveEmpaquetado.Text = "Desempaquetando '" + fileName + "'...";
 
            progressBarEncrypt.Maximum = (int)totalBytes;
            progressBarEncrypt.Value = (int)recvBytes;
            labelInfoEncrypt.Text = (int)((100.0 / progressBarEncrypt.Maximum) * progressBarEncrypt.Value) + "%";
            if (m_DialogMode == EncryptDialogMode.InputPasswordEncrypt) etiquetaDeConfirmarProgreso.Text = "Encriptando empleando '" + System.Enum.GetName(typeof(CriptoFileMethod), method) + "'...";
            else etiquetaDeConfirmarProgreso.Text = "Desencriptando empleando '" + System.Enum.GetName(typeof(CriptoFileMethod), method) + "'...";
        }

        private void OnClickButtonCancel(object sender, EventArgs e)
        {

        }
    } 
}